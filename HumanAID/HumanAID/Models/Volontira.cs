﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HumanAID.Models
{
    public class Volontira
    {
        public int Id { get; set; }

        public String IdVolontera { get; set; }
        
        public int IdDogadjaja { get; set; }
       

         [ForeignKey("IdVolontera")]
         public virtual ApplicationUser Volonter { get; set; }

        [ForeignKey("IdDogadjaja")]
        public virtual Dogadjaj Dogadjaj { get; set; }
    }
}