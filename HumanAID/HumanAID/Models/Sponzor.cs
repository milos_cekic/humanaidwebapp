﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HumanAID.Models
{
    public class Sponzor : Korisnik
    {
        [Required(ErrorMessage = "Molimo Vas unesite naziv Vaše kompanije.")]
        [StringLength(100)]
        [Display(Name ="Naziv kompanije")]
        public String Naziv { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite datum osnivanja kompanije.")]
        [Display(Name ="Datum osnivanja kompanije")]
        public DateTime DatumOsnivanja { get; set; }


        [Required(ErrorMessage = "Molimo Vas unesite delatnost kojom se bavi Vaša kompanija.")]
        [StringLength(255)]
        [Display(Name ="Delatnost kojom se bavite")]
        public String Delatnost { get; set; }


        public override Korisnik VratiKorisnika(int id)
        {
            return this;
            //throw new NotImplementedException();
        }

        public override Korisnik AzurirajKorisnika(int id)
        {
            throw new NotImplementedException();
        }
    }
}