﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HumanAID.Models
{
    public class HumanitarnaOrganizacija : Korisnik
    {
        [Required(ErrorMessage = "Molimo Vas unesite naziv humanitarne organizacije.")]
        [StringLength(255)]
        [Display(Name ="Naziv humanitarne organizacije")]
        public String NazivOrganizacije { get; set; }

        [Display(Name ="Datum osnivanja humanitarne organizacije")]
        [Required(ErrorMessage ="Molimo Vas unesite datum osnivanja humanitarne organizacije.")]
        public DateTime DatumOsnivanjaOrganizacije { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite glavni račun humanitarne organizacije.")]
        [StringLength(255)]
        [Display(Name ="Glavni račun humanitarne organizacije")]
        public String GlavniRacun { get; set; }


        public override Korisnik VratiKorisnika(int id)
        {
            //if (Id == id)
            return this;
            //else
            // return;
            //throw new NotImplementedException();
            //proveriti da li moze neki exception da se baci//mozda moze u kontroleru da se proverava i da se baci http.NotFound ili 404 nesto
        }

        public override Korisnik AzurirajKorisnika(int id)
        {
            throw new NotImplementedException();
        }
    }
}