﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HumanAID.Models
{
    public class Administrator : Korisnik
    {

        [Required]
        [StringLength(25)]
        public String Ime { get; set; }

        [Required]
        [StringLength(35)]
        public String Prezime { get; set; }

        [Required]
        public DateTime DatumRodjenja { get; set; }

        public String Pol { get; set; }        

        //ove funkcije nam najverovatnije ne trebaju uopste
        public override Korisnik AzurirajKorisnika(int id)
        {
            throw new NotImplementedException();
        }

        public override Korisnik VratiKorisnika(int id)
        {
            return this;
            //throw new NotImplementedException();
        }
        //treba da sredimo administratora, da i on moze da menja svoj profil
    }
}