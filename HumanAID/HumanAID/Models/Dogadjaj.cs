﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity;

namespace HumanAID.Models
{

    public enum FazaDogadjaja
    {
        [Display(Name = @"U pripremi")]
        UPripremi =0,
        [Display(Name = @"Aktuelni")]
        Aktuelni =1,
        [Display(Name = @"Prošli")]
        Prosli =2
    }


    public class Dogadjaj
    {

        public int Id { get; set; }

        [Required(ErrorMessage ="Molimo Vas unesite naziv humanitarnog događaja.")]
        [Display(Name ="Naziv humanitarnog događaja")]
        [StringLength(255)]
        public String NazivDogadjaja { get; set; }

        [Display(Name ="Mesto održavanja")]
        public String MestoOdrzavanja { get; set; }

        [Display(Name = "Datum početka događaja")]
        [Required(ErrorMessage = "Molimo Vas unesite datum početka događaja.")]
        public DateTime DatumPocetkaDogadjaja { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite datum završetka događaja.")]
        [Display(Name = "Datum završetka događaja")]
        public DateTime DatumKrajaDogadjaja { get; set; }


        public virtual IEnumerable<Roba> PotrebnaRoba { get; set; }

        public int? BrojPotrebnihVolontera { get; set; }
        public int? BrojTrenutnihVolontera { get; set; }

        [Display(Name = "Žiro račun za realizaciju događaja")]
        //ovaj ziro racun je striktno vezan za dogadjaj, za organizaciju dogadjaja ili neka akcija
        public String ZiroRacun { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite opis humanitarnog događaja.")]
        [Display(Name = "Opis događaja")]
        public String Opis { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite fazu u kojoj se nalazi humanitarni događaj.")]
        [Display(Name = "Faza u kojoj se nalazi događaj")]
        public FazaDogadjaja Faza { get; set; }

        public String IdKaOrganizaciji { get; set; }


        [ForeignKey("IdKaOrganizaciji")]
        public virtual ApplicationUser Organizacija { get; set; }

        public int? IdPotrebnogNovca { get; set; }
       
        //treba da sredimo u dogadjaj controlleru za postavljanje,update i brisanje dogadjaja
        public DateTime? DatumPostavljanja { get; set; }

        public string UrlSlike { get; set; }

        [NotMapped]
        public HttpPostedFileBase UploadImage { get; set; }

        public bool MozeDaSePovecaBrTrenutnihVolontera()
        {
            if (BrojTrenutnihVolontera == null && BrojPotrebnihVolontera == null)
                return false;
            else if (BrojTrenutnihVolontera == null)
            {
                //da bi moglo da se poveca
                BrojTrenutnihVolontera = 0;
                return true;
            }
            else if (BrojTrenutnihVolontera < BrojPotrebnihVolontera)
                return true;
            else
                return false;


        }

        //posto oba mogu da budu nu
        public bool MozeDaSeSmanjiBrTrenutnihVolontera()
        {
            if (BrojTrenutnihVolontera > 0 && BrojPotrebnihVolontera != null)
                return true;
            else
                return false;

        }
        //ova funkcija je napravljena da bi je pozivali u racunanje progresa
        public bool IspunjenUslovZaVolontere()
        {
            if (BrojPotrebnihVolontera == null)
                return true;
            else if (BrojPotrebnihVolontera <= BrojTrenutnihVolontera)
                return true;
            else
                return false;
        }
    }
}