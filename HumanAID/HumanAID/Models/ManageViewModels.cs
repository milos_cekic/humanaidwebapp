﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace HumanAID.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "Šifra {0} treba da bude minimum {2} karaktera duga.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nova šifra")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potvrdite šifru")]
        [Compare("NewPassword", ErrorMessage = "Šifre se ne poklapaju.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Trenutna šifra")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Šifra {0} treba da bude minimum {2} karaktera duga.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nova šifra")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potvrdite novu šifru")]
        [Compare("NewPassword", ErrorMessage = "Šifre se ne poklapaju.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Kontakt telefon")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        //ovo je mozda pozivni broj
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Kontakt telefon")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
}