﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HumanAID.Models
{
    public abstract class Korisnik
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Molimo Vas unesite Vaše korisničko ime.")]
        [StringLength(35)]
        [Display(Name ="Korisničko ime")]
        public String Username { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vašu email adresu.")]
        [StringLength(100)]
        public String Email { get; set; }
        

        [Required(ErrorMessage = "Molimo Vas unesite Vaš broj telefona.")]
        [StringLength(255)]
        [Display(Name ="Kontakt telefon")]
        public String Kontakt { get; set; }

        public String Adresa { get; set; }

        public String Grad { get; set; }

        public String Opis { get; set; }

        [Display(Name ="Link ka sajtu")]
        public String LinkKaSajtu { get; set; }

        //cuva se putanja do te slike
        [Display(Name ="Putanja do Vaše slike")]
        public String LogoPutanja { get; set; }

        public DateTime DatumRegistracijeKorisnika { get; set; }


        public abstract Korisnik VratiKorisnika(int id);
        public abstract Korisnik AzurirajKorisnika(int id);

    }
}