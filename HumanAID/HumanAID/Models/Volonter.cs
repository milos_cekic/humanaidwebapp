﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HumanAID.Models
{


    public class Volonter : Korisnik
    {
        [Required(ErrorMessage = "Molimo Vas unesite Vaše ime.")]
        [StringLength(25)]
        public String Ime { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše prezime.")]
        [StringLength(35)]
        public String Prezime { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite datum rodjenja.")]
        [Display(Name ="Datum rodjenja")]
        [Min16GodinaVolonter]
        public DateTime DatumRodjenja { get; set; }

        [Display(Name ="Izaberite pol")]
        public String Pol { get; set; }

        
        public override Korisnik VratiKorisnika(int id)
        {
            return this;
            //throw new NotImplementedException();
        }

        public override Korisnik AzurirajKorisnika(int id)
        {
            throw new NotImplementedException();
        }
    }
}