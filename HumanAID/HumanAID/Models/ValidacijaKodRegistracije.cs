﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HumanAID.Models
{
    public class ValidacijaKodRegistracije : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (validationContext.ObjectInstance is ApplicationUser)
            {
                var korisnik = (ApplicationUser)validationContext.ObjectInstance;

                if (korisnik.KorisnikTip== TipKorisnika.Organizacija)
                {
                    //odavde ide za humanitarnu organizaciju


                    if (korisnik.NazivOrganizacije == null || korisnik.NazivOrganizacije == "")
                    {
                        return new ValidationResult("Molimo Vas unesite naziv humanitarne organizacije.");

                    }

                    else if (korisnik.DatumOsnivanja == null)
                    {
                        return new ValidationResult("Molimo Vas unesite datum osnivanja humanitarne organizacije.");
                    }

                    else if (korisnik.GlavniRacun == null || korisnik.GlavniRacun == "")
                    {
                        return new ValidationResult("Molimo Vas unesite glavni račun humanitarne organizacije.");
                    }
                    else
                        return ValidationResult.Success;
                }

                else if (korisnik.KorisnikTip == TipKorisnika.Sponzor)
                {

                    if (korisnik.Naziv == null || korisnik.Naziv == "")
                    {
                        return new ValidationResult("Molimo Vas unesite naziv Vaše kompanije.");

                    }

                    else if (korisnik.DatumOsnivanja == null)
                    {
                        return new ValidationResult("Molimo Vas unesite datum osnivanja kompanije.");

                    }

                    else if (korisnik.Delatnost == null || korisnik.Delatnost == "")
                    {
                        return new ValidationResult("Molimo Vas unesite delatnost kojom se bavi Vaša kompanija.");
                    }
                    else
                        return ValidationResult.Success;
                }

                else
                {
                    //mora da bude volonter 

                    if (korisnik.Ime == null || korisnik.Ime == "")
                    {
                        return new ValidationResult("Molimo Vas unesite Vaše ime.");
                    }
                    else if (korisnik.Prezime == null || korisnik.Prezime == "")
                    {
                        return new ValidationResult("Molimo Vas unesite Vaše prezime.");
                    }
                    else if (korisnik.DatumRodjenja == null)
                    {
                        return new ValidationResult("Molimo Vas unesite datum rodjenja.");
                    }

                    else if (korisnik.Pol == null || korisnik.Pol == "")
                    {
                        return new ValidationResult("Molimo Vas izaberite pol.");
                    }
                    
                    ////racunanje godina volontera, mora imati preko 16
                    //int godina = DateTime.Now.Year - korisnik.DatumRodjenja.Year;

                    //if (DateTime.Now.Month == korisnik.DatumRodjenja.Month &&
                    //    DateTime.Now.Day < volonter.DatumRodjenja.Day
                    //    || DateTime.Now.Month < volonter.DatumRodjenja.Month)
                    //{
                    //    godina--;
                    //}

                    //if (godina >= 16)
                    //    return ValidationResult.Success;
                    //else
                    //    return new ValidationResult("Morate biti stariji od 16 godina.");


                    else
                        return ValidationResult.Success;
                }
            }
            else
                return new ValidationResult("Ne postoji taj korisnik");
        }
    }
}