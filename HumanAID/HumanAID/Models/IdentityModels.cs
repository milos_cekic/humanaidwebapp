﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace HumanAID.Models
{
    public enum TipKorisnika
    {
        [Display(Name = @"Humanitarna organizacija")]
        Organizacija = 0,
        [Display(Name = @"Volonter")]
        Volonter = 1,
        [Display(Name = @"Sponzor")]
        Sponzor = 2
    }

    

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [NotMapped]
        public HttpPostedFileBase UploadImage { get; set; }


        [Required(ErrorMessage = "Molimo Vas odaberite tip korisnika.")]
        [Display(Name = "Tipovi Korisnika:")]
        public TipKorisnika KorisnikTip { get; set; }

        //administrator ima iste propertije kao i volonter: ime, prezime, datumRodjenja i pol
        //postojace samo jedan administrator i ovo je flag za njega
        public bool Administrator { get; set; }
        //on ce uvek da bude false samo prvi put kada budemo ubacivali adminisratora bice true

        //pocinje abstract Korisnik
        public String Adresa { get; set; }

        public String Grad { get; set; }

        public String Opis { get; set; }

        [Display(Name = "Link ka sajtu")]
        public String LinkKaSajtu { get; set; }

        //cuva se putanja do te slike, ovo cemo da vidimo kako cemo da resimo sliku
        //to cemo naknadno da resimo
        [Display(Name = "Putanja do Vaše slike")]
        public String LogoPutanja { get; set; }

        public DateTime DatumRegistracijeKorisnika { get; set; }
        //do ovde je bila abstract Korisnik klasa, svi imaju ove zajednicke propertije

        //odavde ide za humanitarnu organizaciju

       // [ValidacijaKodRegistracije]
        [StringLength(255)]
        [Display(Name = "Naziv humanitarne organizacije")]
        public String NazivOrganizacije { get; set; }

        [Display(Name = "Datum osnivanja humanitarne organizacije")]
        //[ValidacijaKodRegistracije]
        public DateTime? DatumOsnivanjaOrganizacije { get; set; }

        //[ValidacijaKodRegistracije]
        [StringLength(255)]
        [Display(Name = "Glavni račun humanitarne organizacije")]
        public String GlavniRacun { get; set; }
        //do ovde je bila humanitarna organizacija

        //odavde pocinje sponzor
        //[ValidacijaKodRegistracije]
        [StringLength(100)]
        [Display(Name = "Naziv kompanije")]
        public String Naziv { get; set; }

        //[ValidacijaKodRegistracije]
        [Display(Name = "Datum osnivanja kompanije")]
        public DateTime? DatumOsnivanja { get; set; }

        //[ValidacijaKodRegistracije]
        [StringLength(255)]
        [Display(Name = "Delatnost kojom se bavite")]
        public String Delatnost { get; set; }
        //do odve je bio sponzor
        //pocinje volonter

        //[ValidacijaKodRegistracije]
        [StringLength(25)]
        public String Ime { get; set; }

        //[ValidacijaKodRegistracije]
        [StringLength(35)]
        public String Prezime { get; set; }

        //[ValidacijaKodRegistracije]
        [Display(Name = "Datum rodjenja")]
        public DateTime? DatumRodjenja { get; set; }

        //[ValidacijaKodRegistracije]
        [Display(Name = "Izaberite pol")]
        public String Pol { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public  DbSet<HumanitarnaOrganizacija> HumanitarneOrganizacije { get; set; }
        public  DbSet<Administrator> Administratori { get; set; }
        public  DbSet<Sponzor> Sponzori { get; set; }
        public  DbSet<Volonter> Volonteri { get; set; }
        public  DbSet<Dogadjaj> Dogadjaji { get; set; }
        public  DbSet<Donira> Donira { get; set; }
        public DbSet<Volontira> Volontira { get; set; }
        public DbSet<Roba> Roba { get; set; }
        public DbSet<PrikupljenaRoba> PrikupljenaRoba { get; set; }
        public DbSet<PotrebanNovac> PotrebanNovac { get; set; }
        public DbSet<NovcaneDonacije> NovcaneDonacije { get; set; }


        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}