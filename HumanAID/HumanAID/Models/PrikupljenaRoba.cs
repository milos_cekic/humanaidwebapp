﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HumanAID.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

public enum TipDoniranja
{
    [Display(Name = @"Kupljeno doniranim novcem")]
    PrikupljenimNovcem = 0,
    [Display(Name = @"Prikupljeno robnim putem")]
    RobnimPutem = 1
}


namespace HumanAID.Models
{
    public class PrikupljenaRoba
    {
        public int Id { get; set; }

        //naziv potrebne robe
        [Required(ErrorMessage = "Molimo Vas unesite naziv proizvoda i NETO proizvoda.")]
        [Display(Name = "Naziv proizvoda i NETO proizvoda.")]
        public String NazivPrikupljeneRobe { get; set; }

        [Required(ErrorMessage = "Molimo Vas odaberite kako ste prikupili potrebnu robu.")]
        [Display(Name = "Način prikupljanja robe")]
        public TipDoniranja DoniranaTip { get; set; }

        //ovde ide naziv organizcije(ako je kupljeno doniranim novcem)
        //ili naziv sponzora
        [Required(ErrorMessage = "Molimo Vas unesite naziv odgovorno lice za prikupljenu robu.")]
        [Display(Name = "Unesite naziv odgovornog lica za prikupljenu robu.")]
        public String PrikupioRobu { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite količinu koju želite da donirate.")]
        [Display(Name = "Količina koju želite da donirate")]
        public int Kolicina { get; set; }

        public int? IdPotrebneRobe { get; set; }

        [ForeignKey("IdPotrebneRobe")]
        public virtual Roba PotrebnaRoba { get; set; }

        public String IdMogucegSponzora { get; set; }

        [ForeignKey("IdMogucegSponzora")]
        public virtual ApplicationUser Sponzor { get; set; }

        public int? IdDogadjaja { get; set; }

        [ForeignKey("IdDogadjaja")]
        public virtual Dogadjaj Dogadjaj { get; set; }
    }
}