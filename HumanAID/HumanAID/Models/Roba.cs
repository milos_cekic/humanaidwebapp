﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HumanAID.Models
{
    public class Roba
    {
        public int Id { get; set; }

        //strani kljuc ka dogadjaju
        public int? DogadjajId { get; set; }

        [ForeignKey("DogadjajId")]
        public virtual Dogadjaj Dogadjaj { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite naziv proizvoda i NETO proizvoda")]
        [Display(Name = "Naziv proizvoda i NETO proizvoda")]
        public String Proizvod { get; set; }

        //trenutno prikupljena kolicina
        public int? PrikupljenaRoba { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite potrebnu količinu proizvoda(u komadima)")]
        [Display(Name = "Potrebna količina proizvoda(u komadima)")]
        public int PotrebnaKolicina { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite cenu proizvoda po komadu")]
        [Display(Name = "Cena proizvoda po komadu")]
        public decimal CenaProizvodaPoKomadu { get; set; }


        public int? IdPotrebnogNovca { get; set; }

        [ForeignKey("IdPotrebnogNovca")]
        public virtual PotrebanNovac PotrebanNovac { get; set; }


        public decimal UkupnaCenaProizvoda()
        {
            return CenaProizvodaPoKomadu * PotrebnaKolicina;
        }
    }
}
