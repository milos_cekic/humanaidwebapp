﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace HumanAID.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Korisničko ime")]
        //[EmailAddress]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Šifra")]
        public string Password { get; set; }

        [Display(Name = "Zapamti me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [NotMapped]
        public HttpPostedFileBase UploadImage { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše korisničko ime.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Šifra {0} mora biti {2} karaktera duga.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Šifra")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potvrdi šifru")]
        [Compare("Password", ErrorMessage = "Šifre se ne poklapaju.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše korisničko ime.")]
        [StringLength(35)]
        [Display(Name = "Korisničko ime")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaš broj telefona.")]
        [StringLength(255)]
        [Display(Name = "Kontakt telefon")]
        public string PhoneNumber { get; set; }


        //ovo su nasi parametri
        [Required(ErrorMessage = "Molimo Vas odaberite tip korisnika.")]
        [Display(Name = "Tipovi Korisnika:")]
        public TipKorisnika KorisnikTip { get; set; }

        //administrator ima iste propertije kao i volonter: ime, prezime, datumRodjenja i pol
        //postojace samo jedan administrator i ovo je flag za njega
        public bool Administrator { get; set; }
        //on ce uvek da bude false samo prvi put kada budemo ubacivali adminisratora bice true

        //pocinje abstract Korisnik
        public String Adresa { get; set; }

        public String Grad { get; set; }

        public String Opis { get; set; }

        [Display(Name = "Link ka sajtu")]
        public String LinkKaSajtu { get; set; }

        //cuva se putanja do te slike, ovo cemo da vidimo kako cemo da resimo sliku
        //to cemo naknadno da resimo
        [Display(Name = "Putanja do Vaše slike")]
        public String LogoPutanja { get; set; }

        public DateTime DatumRegistracijeKorisnika { get; set; }
        //do ovde je bila abstract Korisnik klasa, svi imaju ove zajednicke propertije

        //odavde ide za humanitarnu organizaciju

//        [ValidacijaKodRegistracije]
        [StringLength(255)]
        [Display(Name = "Naziv humanitarne organizacije")]
        public String NazivOrganizacije { get; set; }

        [Display(Name = "Datum osnivanja humanitarne organizacije")]
  //      [ValidacijaKodRegistracije]
        public DateTime? DatumOsnivanjaOrganizacije { get; set; }

    //    [ValidacijaKodRegistracije]
        [StringLength(255)]
        [Display(Name = "Glavni račun humanitarne organizacije")]
        public String GlavniRacun { get; set; }
        //do ovde je bila humanitarna organizacija

        //odavde pocinje sponzor
      //  [ValidacijaKodRegistracije]
        [StringLength(100)]
        [Display(Name = "Naziv kompanije")]
        public String Naziv { get; set; }

        ///[ValidacijaKodRegistracije]
        [Display(Name = "Datum osnivanja kompanije")]
        public DateTime? DatumOsnivanja { get; set; }

//        [ValidacijaKodRegistracije]
        [StringLength(255)]
        [Display(Name = "Delatnost kojom se bavite")]
        public String Delatnost { get; set; }
        //do odve je bio sponzor
        //pocinje volonter

  //      [ValidacijaKodRegistracije]
        [StringLength(25)]
        public String Ime { get; set; }

    //    [ValidacijaKodRegistracije]
        [StringLength(35)]
        public String Prezime { get; set; }

      //  [ValidacijaKodRegistracije]
        [Display(Name = "Datum rodjenja")]
        public DateTime? DatumRodjenja { get; set; }

        //[ValidacijaKodRegistracije]
        [Display(Name = "Izaberite pol")]
        public String Pol { get; set; }

      
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Šifra {0} mora biti {2} karaktera duga.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Šifra")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potvrdi šifru")]
        [Compare("Password", ErrorMessage = "Šifre se ne poklapaju.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
