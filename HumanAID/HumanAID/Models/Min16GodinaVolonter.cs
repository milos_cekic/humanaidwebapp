﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using HumanAID.Dtos;

namespace HumanAID.Models
{
    public class Min16GodinaVolonter: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (validationContext.ObjectInstance is Volonter)
            {
                var volonter = (Volonter)validationContext.ObjectInstance;

                if (volonter.DatumRodjenja == null)
                    return new ValidationResult("Datum rodjenja je obavezan.");

                //racunanje godina volontera
                int godina = DateTime.Now.Year - volonter.DatumRodjenja.Year;

                if (DateTime.Now.Month == volonter.DatumRodjenja.Month &&
                    DateTime.Now.Day < volonter.DatumRodjenja.Day
                    || DateTime.Now.Month < volonter.DatumRodjenja.Month)
                {
                    godina--;
                }

                if (godina >= 16)
                    return ValidationResult.Success;
                else
                    return new ValidationResult("Morate biti stariji od 16 godina.");

            }
            else
            {
                var volonterDto = (VolonterDto)validationContext.ObjectInstance;

                if (volonterDto.DatumRodjenja == null)
                    return new ValidationResult("Datum rodjenja je obavezan.");

                //racunanje godina volontera
                int godina = DateTime.Now.Year - volonterDto.DatumRodjenja.Year;

                if (DateTime.Now.Month == volonterDto.DatumRodjenja.Month &&
                    DateTime.Now.Day < volonterDto.DatumRodjenja.Day
                    || DateTime.Now.Month < volonterDto.DatumRodjenja.Month)
                {
                    godina--;
                }

                if (godina >= 16)
                    return ValidationResult.Success;
                else
                    return new ValidationResult("Morate biti stariji od 16 godina.");

            }
        }

    }

}

