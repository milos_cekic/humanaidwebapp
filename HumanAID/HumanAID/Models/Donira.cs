﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HumanAID.Models
{
    public class Donira
    {
        public int Id { get; set; }

        public String IdSponzora { get; set; }

        public int IdDogadjaja { get; set; }

        [ForeignKey("IdSponzora")]
        public virtual ApplicationUser Sponzor { get; set; }

        [ForeignKey("IdDogadjaja")]
        public virtual Dogadjaj Dogadjaj { get; set; }

    }
}