﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HumanAID.Models;

namespace HumanAID.ViewModels
{
    public class PrikazDogadjajaViewModel
    {
        public Dogadjaj Dogadjaj { get; set; }
        public ApplicationUser Organizacija { get; set; }
        public IEnumerable<Donira> ListaDonira { get; set; }
        public IEnumerable<Volontira> ListaVolontira { get; set; }
        public IEnumerable<Roba> ListaPotrebneRobe { get; set; }
        public IEnumerable<PrikupljenaRoba> ListaPrikupljeneRobe { get; set; }
        public string IdTrenutnogUlogovanogKorisnika { get; set; }
        public int IdVolontira { get; set; }
        public int IdDonira { get; set; }
        public decimal ProcenatIspunjenosti { get; set; }
        public IEnumerable<PrikupljenaRoba> ListaPrikupljeneRobeUlogovanogSponzora { get; set; }
        public PotrebanNovac PotrebanNovac { get; set; }
        public IEnumerable<NovcaneDonacije> ListaPrikupljenogNovcaUlogovanogSponzora { get; set; }
    }
}