﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HumanAID.Models;

namespace HumanAID.ViewModels
{
    //bitno da se ovde vidi da probamo da ovo bude lista volontira
    //ako tako hoce da radi onda ce i donira da se radi na tom principu
    //u stavri ne bas tako jer tamo ima prikupljena roba/roba prikupljennovac/novac
    //pa cemo to da razradimo
    public class ProfilVolonteraViewModel
    {
        public ApplicationUser Volonter { get; set; }
        public IEnumerable<Volontira> ListaVolontira { get; set; }
    }
}