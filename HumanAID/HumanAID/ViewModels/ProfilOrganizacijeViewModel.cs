﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HumanAID.Models;
using System.Data.Entity;

namespace HumanAID.ViewModels
{
    public class ProfilOrganizacijeViewModel
    {
        public ApplicationUser Organizacija { get; set; }
        public IEnumerable<Dogadjaj> ListaOrganizovanihDogadjaja { get; set; }
        public Dogadjaj DogadjajPut { get; set; }
        public List<bool> MozeDaSeKupiOstatak { get; set; }
    }
}