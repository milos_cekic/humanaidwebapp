﻿using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HumanAID.ViewModels
{
    public class AktuelniDogadjajiViewModel
    {
        public IEnumerable<Dogadjaj> ListaAktuelnihDogadjaja { get; set; }
        public List<decimal> ProcenatIspunjenosti { get; set; }
    }
}