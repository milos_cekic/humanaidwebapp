﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HumanAID.Models;

namespace HumanAID.ViewModels
{
    //za realizovanje view i sredjivanje u controlleru i testiranje cemo kasnije da pogledamo
    //ovo je ovde samo da bi se videla logika koja ide kao i za ostale viewmodele
    public class PotrebnaRobaZaDogadjajViewModel
    {
        public Dogadjaj Dogadjaj { get; set; }
        public IEnumerable<Roba> ListaPotrebneRobe { get; set; }


        //minimalniBudzet=ukupniTroskoviRobeUNovcu;
        //minimalniBudzetPrikupljeno=trenutnoPrikupljenaRobaUNovcu;


        //ovo moramo da razresimo kako cemo da implementiramo celu logiku za novac i za robu
        //ako je minimalniBudzet=ukupniTroskoviRobeUNovcu onda promena jednog utice i na promenu drugog
        //sto znaci da ova funkcija izracunaj progres nije tacna


        //ovde je stavljeno kao place holder, implementacija ce najverovatnije biti na drugom mestu
        //ovo je glavna funkcija za izracuvanje progresa konkretno svih roba, zbog onog glavnog bara koji prikazuje % (progres)
        //public double IzracuajProgres()
        //{
        //    double rezultat = 0;
        //    int trenutnaKolicinaSvihRoba = 0;
        //    int potrebnaKolicinaSvihRoba = 0;

        //    foreach (Roba r in PotrebnaRoba)
        //    {
        //        trenutnaKolicinaSvihRoba += r.TrenutnaKolicina;
        //    }

        //    foreach (Roba r in PotrebnaRoba)
        //    {
        //        potrebnaKolicinaSvihRoba += r.PotrebnaKolicina;
        //    }

        //    rezultat = (MinimalniBudzetPrikupljeno / MinimalniBudzet + trenutnaKolicinaSvihRoba / potrebnaKolicinaSvihRoba) * 100 / 2;
        //    //delimo sa 2 jer ako je sve ispunjeno bice na kraju kao da je 200%

        //    if (rezultat >= 100)
        //        rezultat = 100;


        //    return rezultat;

        //}
        ////ispitivanja da li je doslo do prekomerne robe cemo da imamo na drugom mestu isto kao i za novcano
        ////mozda visak robe i ne treba
    }
}