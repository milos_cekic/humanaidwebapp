﻿using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HumanAID.ViewModels
{
    public class ProsliDogadjajiViewModel
    {
        public IEnumerable<Dogadjaj> ListaProslihDogadjaja { get; set; }
        public List<decimal> ProcenatIspunjenosti { get; set; }
    }
}