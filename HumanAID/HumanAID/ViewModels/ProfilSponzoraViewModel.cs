﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HumanAID.Models;

namespace HumanAID.ViewModels
{
    public class ProfilSponzoraViewModel
    {
        public ApplicationUser Sponzor { get; set; }
        public IEnumerable<Donira> ListaDonira { get; set; }
    }
}