namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezStatusa : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Volonters", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Volonters", "Status", c => c.String());
        }
    }
}
