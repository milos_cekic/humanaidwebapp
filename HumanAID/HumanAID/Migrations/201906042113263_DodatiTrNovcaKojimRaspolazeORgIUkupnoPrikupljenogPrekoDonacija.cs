namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatiTrNovcaKojimRaspolazeORgIUkupnoPrikupljenogPrekoDonacija : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PotrebanNovacs", "UkupnoPrikupljenaKolicinaNovca", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.PotrebanNovacs", "TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PotrebanNovacs", "TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje");
            DropColumn("dbo.PotrebanNovacs", "UkupnoPrikupljenaKolicinaNovca");
        }
    }
}
