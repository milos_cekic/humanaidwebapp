namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatiAtributiZaKorisnikeUApplicationUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Adresa", c => c.String());
            AddColumn("dbo.AspNetUsers", "Grad", c => c.String());
            AddColumn("dbo.AspNetUsers", "Opis", c => c.String());
            AddColumn("dbo.AspNetUsers", "LinkKaSajtu", c => c.String());
            AddColumn("dbo.AspNetUsers", "LogoPutanja", c => c.String());
            AddColumn("dbo.AspNetUsers", "DatumRegistracijeKorisnika", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "NazivOrganizacije", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.AspNetUsers", "DatumOsnivanjaOrganizacije", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "GlavniRacun", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.AspNetUsers", "Naziv", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AspNetUsers", "DatumOsnivanja", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "Delatnost", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.AspNetUsers", "Ime", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.AspNetUsers", "Prezime", c => c.String(nullable: false, maxLength: 35));
            AddColumn("dbo.AspNetUsers", "DatumRodjenja", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "Pol", c => c.String());
            AddColumn("dbo.AspNetUsers", "BrojBedzeva", c => c.Int());
            AlterColumn("dbo.AspNetUsers", "Email", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.AspNetUsers", "PhoneNumber", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "PhoneNumber", c => c.String());
            AlterColumn("dbo.AspNetUsers", "Email", c => c.String(maxLength: 256));
            DropColumn("dbo.AspNetUsers", "BrojBedzeva");
            DropColumn("dbo.AspNetUsers", "Pol");
            DropColumn("dbo.AspNetUsers", "DatumRodjenja");
            DropColumn("dbo.AspNetUsers", "Prezime");
            DropColumn("dbo.AspNetUsers", "Ime");
            DropColumn("dbo.AspNetUsers", "Delatnost");
            DropColumn("dbo.AspNetUsers", "DatumOsnivanja");
            DropColumn("dbo.AspNetUsers", "Naziv");
            DropColumn("dbo.AspNetUsers", "GlavniRacun");
            DropColumn("dbo.AspNetUsers", "DatumOsnivanjaOrganizacije");
            DropColumn("dbo.AspNetUsers", "NazivOrganizacije");
            DropColumn("dbo.AspNetUsers", "DatumRegistracijeKorisnika");
            DropColumn("dbo.AspNetUsers", "LogoPutanja");
            DropColumn("dbo.AspNetUsers", "LinkKaSajtu");
            DropColumn("dbo.AspNetUsers", "Opis");
            DropColumn("dbo.AspNetUsers", "Grad");
            DropColumn("dbo.AspNetUsers", "Adresa");
        }
    }
}
