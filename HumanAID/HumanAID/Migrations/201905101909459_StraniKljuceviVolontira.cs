namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StraniKljuceviVolontira : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Volontiras",
                c => new
                    {
                        IdVolontera = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdVolontera, t.IdDogadjaja })
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja, cascadeDelete: true)
                .ForeignKey("dbo.Volonters", t => t.IdVolontera, cascadeDelete: true)
                .Index(t => t.IdVolontera)
                .Index(t => t.IdDogadjaja);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Volontiras", "IdVolontera", "dbo.Volonters");
            DropForeignKey("dbo.Volontiras", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.Volontiras", new[] { "IdDogadjaja" });
            DropIndex("dbo.Volontiras", new[] { "IdVolontera" });
            DropTable("dbo.Volontiras");
        }
    }
}
