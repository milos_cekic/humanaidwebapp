namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezIdKaOrg : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Dogadjajs", "IdOrganizacije");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dogadjajs", "IdOrganizacije", c => c.Int());
        }
    }
}
