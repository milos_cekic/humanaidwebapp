namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdSponzoraKodPrikupljeneRobeKaUseru : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrikupljenaRobas", "IdMogucegSponzora", c => c.String(maxLength: 128));
            CreateIndex("dbo.PrikupljenaRobas", "IdMogucegSponzora");
            AddForeignKey("dbo.PrikupljenaRobas", "IdMogucegSponzora", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdMogucegSponzora", "dbo.AspNetUsers");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdMogucegSponzora" });
            DropColumn("dbo.PrikupljenaRobas", "IdMogucegSponzora");
        }
    }
}
