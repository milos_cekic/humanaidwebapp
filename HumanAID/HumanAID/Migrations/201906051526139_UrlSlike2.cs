namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UrlSlike2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "UrlSlike", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dogadjajs", "UrlSlike");
        }
    }
}
