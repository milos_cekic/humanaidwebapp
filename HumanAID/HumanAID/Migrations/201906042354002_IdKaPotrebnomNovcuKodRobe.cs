namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdKaPotrebnomNovcuKodRobe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Robas", "IdPotrebnogNovca", c => c.Int());
            CreateIndex("dbo.Robas", "IdPotrebnogNovca");
            AddForeignKey("dbo.Robas", "IdPotrebnogNovca", "dbo.PotrebanNovacs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Robas", "IdPotrebnogNovca", "dbo.PotrebanNovacs");
            DropIndex("dbo.Robas", new[] { "IdPotrebnogNovca" });
            DropColumn("dbo.Robas", "IdPotrebnogNovca");
        }
    }
}
