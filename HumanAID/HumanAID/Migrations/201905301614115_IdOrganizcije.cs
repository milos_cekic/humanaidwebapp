namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdOrganizcije : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "IdOrganizacije", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dogadjajs", "IdOrganizacije");
        }
    }
}
