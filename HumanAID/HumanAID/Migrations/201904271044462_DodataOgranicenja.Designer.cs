// <auto-generated />
namespace HumanAID.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DodataOgranicenja : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DodataOgranicenja));
        
        string IMigrationMetadata.Id
        {
            get { return "201904271044462_DodataOgranicenja"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
