namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StraniKljucDogadjaja : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Dogadjajs", "IdOrganizacije");
            AddForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas");
            DropIndex("dbo.Dogadjajs", new[] { "IdOrganizacije" });
        }
    }
}
