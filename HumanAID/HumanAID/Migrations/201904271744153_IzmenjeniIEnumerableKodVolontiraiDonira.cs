namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IzmenjeniIEnumerableKodVolontiraiDonira : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs");
            DropForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors");
            DropIndex("dbo.Doniras", new[] { "Dogadjaj_Id" });
            DropIndex("dbo.Doniras", new[] { "Sponzor_Id" });
            DropColumn("dbo.Doniras", "Dogadjaj_Id");
            DropColumn("dbo.Doniras", "Sponzor_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Doniras", "Sponzor_Id", c => c.Int());
            AddColumn("dbo.Doniras", "Dogadjaj_Id", c => c.Int());
            CreateIndex("dbo.Doniras", "Sponzor_Id");
            CreateIndex("dbo.Doniras", "Dogadjaj_Id");
            AddForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors", "Id");
            AddForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs", "Id");
        }
    }
}
