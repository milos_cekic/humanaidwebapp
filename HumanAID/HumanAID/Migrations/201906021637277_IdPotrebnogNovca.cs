namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdPotrebnogNovca : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "IdPotrebnogNovca", c => c.Int());
            AlterColumn("dbo.PotrebanNovacs", "PotrebnaKolicinaNovca", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PotrebanNovacs", "PotrebnaKolicinaNovca", c => c.Single());
            DropColumn("dbo.Dogadjajs", "IdPotrebnogNovca");
        }
    }
}
