namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodataPrikupljenaRoba2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PrikupljenaRobas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivPrikupljeneRobe = c.String(nullable: false),
                        DoniranaTip = c.Int(nullable: false),
                        PrikupioRobu = c.String(nullable: false),
                        Kolicina = c.Int(nullable: false),
                        IdPotrebneRobe = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Robas", t => t.IdPotrebneRobe, cascadeDelete: false)
                .Index(t => t.IdPotrebneRobe);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdPotrebneRobe", "dbo.Robas");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdPotrebneRobe" });
            DropTable("dbo.PrikupljenaRobas");
        }
    }
}
