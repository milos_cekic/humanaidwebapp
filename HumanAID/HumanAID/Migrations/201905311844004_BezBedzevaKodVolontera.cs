namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezBedzevaKodVolontera : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Volonters", "BrojBedzeva");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Volonters", "BrojBedzeva", c => c.Int());
        }
    }
}
