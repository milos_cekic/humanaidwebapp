namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatiParametriUApplicationUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "KorisnikTip", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Administrator", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "Adresa", c => c.String());
            AddColumn("dbo.AspNetUsers", "Grad", c => c.String());
            AddColumn("dbo.AspNetUsers", "Opis", c => c.String());
            AddColumn("dbo.AspNetUsers", "LinkKaSajtu", c => c.String());
            AddColumn("dbo.AspNetUsers", "LogoPutanja", c => c.String());
            AddColumn("dbo.AspNetUsers", "DatumRegistracijeKorisnika", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "NazivOrganizacije", c => c.String(maxLength: 255));
            AddColumn("dbo.AspNetUsers", "DatumOsnivanjaOrganizacije", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "GlavniRacun", c => c.String(maxLength: 255));
            AddColumn("dbo.AspNetUsers", "Naziv", c => c.String(maxLength: 100));
            AddColumn("dbo.AspNetUsers", "DatumOsnivanja", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "Delatnost", c => c.String(maxLength: 255));
            AddColumn("dbo.AspNetUsers", "Ime", c => c.String(maxLength: 25));
            AddColumn("dbo.AspNetUsers", "Prezime", c => c.String(maxLength: 35));
            AddColumn("dbo.AspNetUsers", "DatumRodjenja", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "Pol", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Pol");
            DropColumn("dbo.AspNetUsers", "DatumRodjenja");
            DropColumn("dbo.AspNetUsers", "Prezime");
            DropColumn("dbo.AspNetUsers", "Ime");
            DropColumn("dbo.AspNetUsers", "Delatnost");
            DropColumn("dbo.AspNetUsers", "DatumOsnivanja");
            DropColumn("dbo.AspNetUsers", "Naziv");
            DropColumn("dbo.AspNetUsers", "GlavniRacun");
            DropColumn("dbo.AspNetUsers", "DatumOsnivanjaOrganizacije");
            DropColumn("dbo.AspNetUsers", "NazivOrganizacije");
            DropColumn("dbo.AspNetUsers", "DatumRegistracijeKorisnika");
            DropColumn("dbo.AspNetUsers", "LogoPutanja");
            DropColumn("dbo.AspNetUsers", "LinkKaSajtu");
            DropColumn("dbo.AspNetUsers", "Opis");
            DropColumn("dbo.AspNetUsers", "Grad");
            DropColumn("dbo.AspNetUsers", "Adresa");
            DropColumn("dbo.AspNetUsers", "Administrator");
            DropColumn("dbo.AspNetUsers", "KorisnikTip");
        }
    }
}
