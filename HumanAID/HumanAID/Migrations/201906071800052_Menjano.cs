namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Menjano : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdDogadjaja", "dbo.Dogadjajs");
            DropForeignKey("dbo.PrikupljenaRobas", "IdPotrebneRobe", "dbo.Robas");
            DropForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdPotrebneRobe" });
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdDogadjaja" });
            DropIndex("dbo.Robas", new[] { "DogadjajId" });
            AlterColumn("dbo.PrikupljenaRobas", "IdPotrebneRobe", c => c.Int());
            AlterColumn("dbo.PrikupljenaRobas", "IdDogadjaja", c => c.Int());
            AlterColumn("dbo.Robas", "DogadjajId", c => c.Int());
            CreateIndex("dbo.PrikupljenaRobas", "IdPotrebneRobe");
            CreateIndex("dbo.PrikupljenaRobas", "IdDogadjaja");
            CreateIndex("dbo.Robas", "DogadjajId");
            AddForeignKey("dbo.PrikupljenaRobas", "IdDogadjaja", "dbo.Dogadjajs", "Id");
            AddForeignKey("dbo.PrikupljenaRobas", "IdPotrebneRobe", "dbo.Robas", "Id");
            AddForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs");
            DropForeignKey("dbo.PrikupljenaRobas", "IdPotrebneRobe", "dbo.Robas");
            DropForeignKey("dbo.PrikupljenaRobas", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.Robas", new[] { "DogadjajId" });
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdDogadjaja" });
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdPotrebneRobe" });
            AlterColumn("dbo.Robas", "DogadjajId", c => c.Int(nullable: false));
            AlterColumn("dbo.PrikupljenaRobas", "IdDogadjaja", c => c.Int(nullable: false));
            AlterColumn("dbo.PrikupljenaRobas", "IdPotrebneRobe", c => c.Int(nullable: false));
            CreateIndex("dbo.Robas", "DogadjajId");
            CreateIndex("dbo.PrikupljenaRobas", "IdDogadjaja");
            CreateIndex("dbo.PrikupljenaRobas", "IdPotrebneRobe");
            AddForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PrikupljenaRobas", "IdPotrebneRobe", "dbo.Robas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PrikupljenaRobas", "IdDogadjaja", "dbo.Dogadjajs", "Id", cascadeDelete: true);
        }
    }
}
