namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatIdKaOrganizacijiUDogadjaju : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "IdOrganizacije", c => c.Int(nullable: false));
            Sql("UPDATE dbo.Dogadjajs SET IdOrganizacije = 3 WHERE Id=1");
            Sql("UPDATE dbo.Dogadjajs SET IdOrganizacije = 3 WHERE Id=2");
            Sql("UPDATE dbo.Dogadjajs SET IdOrganizacije = 4 WHERE Id=3");

        }
        
        public override void Down()
        {
            DropColumn("dbo.Dogadjajs", "IdOrganizacije");
        }
    }
}
