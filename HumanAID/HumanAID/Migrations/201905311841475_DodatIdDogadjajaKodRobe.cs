namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatIdDogadjajaKodRobe : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Robas", "DogadjajId");
            AddForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs", "Id", cascadeDelete: true);
            DropColumn("dbo.Robas", "TrenutnaKolicina");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Robas", "TrenutnaKolicina", c => c.Int());
            DropForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs");
            DropIndex("dbo.Robas", new[] { "DogadjajId" });
        }
    }
}
