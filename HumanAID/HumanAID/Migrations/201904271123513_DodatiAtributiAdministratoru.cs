namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatiAtributiAdministratoru : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Administrators", "Ime", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.Administrators", "Prezime", c => c.String(nullable: false, maxLength: 35));
            AddColumn("dbo.Administrators", "DatumRodjenja", c => c.DateTime(nullable: false));
            AddColumn("dbo.Administrators", "Pol", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Administrators", "Pol");
            DropColumn("dbo.Administrators", "DatumRodjenja");
            DropColumn("dbo.Administrators", "Prezime");
            DropColumn("dbo.Administrators", "Ime");
        }
    }
}
