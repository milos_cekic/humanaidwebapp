namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VratiliSmoDoniraISrediliGreske : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Doniras",
                c => new
                    {
                        IdSponzora = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                        Dogadjaj_Id = c.Int(),
                        Sponzor_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.IdSponzora, t.IdDogadjaja })
                .ForeignKey("dbo.Dogadjajs", t => t.Dogadjaj_Id)
                .ForeignKey("dbo.Sponzors", t => t.Sponzor_Id)
                .Index(t => t.Dogadjaj_Id)
                .Index(t => t.Sponzor_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors");
            DropForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs");
            DropIndex("dbo.Doniras", new[] { "Sponzor_Id" });
            DropIndex("dbo.Doniras", new[] { "Dogadjaj_Id" });
            DropTable("dbo.Doniras");
        }
    }
}
