namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatePotrebneDonacije2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NovcaneDonacijes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdSponzora = c.String(maxLength: 128),
                        IdDogadjaja = c.Int(nullable: false),
                        IdPotrebnogNovca = c.Int(nullable: false),
                        NazivSponzora = c.String(),
                        KolicinaNovca = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja, cascadeDelete: false)
                .ForeignKey("dbo.PotrebanNovacs", t => t.IdPotrebnogNovca, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.IdSponzora, cascadeDelete: false)
                .Index(t => t.IdSponzora)
                .Index(t => t.IdDogadjaja)
                .Index(t => t.IdPotrebnogNovca);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NovcaneDonacijes", "IdSponzora", "dbo.AspNetUsers");
            DropForeignKey("dbo.NovcaneDonacijes", "IdPotrebnogNovca", "dbo.PotrebanNovacs");
            DropForeignKey("dbo.NovcaneDonacijes", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.NovcaneDonacijes", new[] { "IdPotrebnogNovca" });
            DropIndex("dbo.NovcaneDonacijes", new[] { "IdDogadjaja" });
            DropIndex("dbo.NovcaneDonacijes", new[] { "IdSponzora" });
            DropTable("dbo.NovcaneDonacijes");
        }
    }
}
