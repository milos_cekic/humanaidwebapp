namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatBrojVolonteraUDogadjaju : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "BrojPotrebnihVolontera", c => c.Int());
            AddColumn("dbo.Dogadjajs", "BrojTrenutnihVolontera", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dogadjajs", "BrojTrenutnihVolontera");
            DropColumn("dbo.Dogadjajs", "BrojPotrebnihVolontera");
        }
    }
}
