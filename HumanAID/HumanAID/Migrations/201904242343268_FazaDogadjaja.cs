namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FazaDogadjaja : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FazaDogadjajas",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Prosli = c.String(),
                        Aktuelni = c.String(),
                        UPripremi = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Dogadjajs", "FazaId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Dogadjajs", "FazaId");
            AddForeignKey("dbo.Dogadjajs", "FazaId", "dbo.FazaDogadjajas", "Id", cascadeDelete: true);
            DropColumn("dbo.Dogadjajs", "Faza");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dogadjajs", "Faza", c => c.Int(nullable: false));
            DropForeignKey("dbo.Dogadjajs", "FazaId", "dbo.FazaDogadjajas");
            DropIndex("dbo.Dogadjajs", new[] { "FazaId" });
            DropColumn("dbo.Dogadjajs", "FazaId");
            DropTable("dbo.FazaDogadjajas");
        }
    }
}
