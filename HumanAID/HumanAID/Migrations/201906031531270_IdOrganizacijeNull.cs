namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdOrganizacijeNull : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas");
            DropIndex("dbo.Dogadjajs", new[] { "IdOrganizacije" });
            AlterColumn("dbo.Dogadjajs", "IdOrganizacije", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dogadjajs", "IdOrganizacije", c => c.Int(nullable: false));
            CreateIndex("dbo.Dogadjajs", "IdOrganizacije");
            AddForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas", "Id", cascadeDelete: true);
        }
    }
}
