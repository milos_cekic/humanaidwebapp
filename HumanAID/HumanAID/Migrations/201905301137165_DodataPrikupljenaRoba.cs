namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodataPrikupljenaRoba : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Dogadjajs", "IdOrganizacije");
            CreateIndex("dbo.Robas", "DogadjajId");
            AddForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs", "Id", cascadeDelete: true);
            DropColumn("dbo.Robas", "TrenutnaKolicina");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Robas", "TrenutnaKolicina", c => c.Int());
            DropForeignKey("dbo.Robas", "DogadjajId", "dbo.Dogadjajs");
            DropForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas");
            DropIndex("dbo.Robas", new[] { "DogadjajId" });
            DropIndex("dbo.Dogadjajs", new[] { "IdOrganizacije" });
        }
    }
}
