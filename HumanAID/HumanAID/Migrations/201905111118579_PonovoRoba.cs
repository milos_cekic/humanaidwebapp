namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PonovoRoba : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Robas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DogadjajId = c.Int(nullable: false),
                        Proizvod = c.String(nullable: false),
                        PotrebnaKolicina = c.Int(nullable: false),
                        TrenutnaKolicina = c.Int(),
                        CenaProizvodaPoKomadu = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Robas");
        }
    }
}
