namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezVolontira : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Volontiras", "Dogadjaj_Id", "dbo.Dogadjajs");
            DropForeignKey("dbo.Volontiras", "Volonter_Id", "dbo.Volonters");
            DropIndex("dbo.Volontiras", new[] { "Dogadjaj_Id" });
            DropIndex("dbo.Volontiras", new[] { "Volonter_Id" });
            DropTable("dbo.Volontiras");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Volontiras",
                c => new
                    {
                        IdVolontera = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                        Dogadjaj_Id = c.Int(),
                        Volonter_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.IdVolontera, t.IdDogadjaja });
            
            CreateIndex("dbo.Volontiras", "Volonter_Id");
            CreateIndex("dbo.Volontiras", "Dogadjaj_Id");
            AddForeignKey("dbo.Volontiras", "Volonter_Id", "dbo.Volonters", "Id");
            AddForeignKey("dbo.Volontiras", "Dogadjaj_Id", "dbo.Dogadjajs", "Id");
        }
    }
}
