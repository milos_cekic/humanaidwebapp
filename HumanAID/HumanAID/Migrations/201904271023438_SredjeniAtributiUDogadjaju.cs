namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SredjeniAtributiUDogadjaju : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Dogadjajs", "FazaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dogadjajs", "FazaId", c => c.Byte(nullable: false));
        }
    }
}
