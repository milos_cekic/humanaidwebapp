namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatPotrebanNovac : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PotrebanNovacs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdDogadjaja = c.Int(nullable: false),
                        PotrebnaKolicinaNovca = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja, cascadeDelete: true)
                .Index(t => t.IdDogadjaja);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PotrebanNovacs", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.PotrebanNovacs", new[] { "IdDogadjaja" });
            DropTable("dbo.PotrebanNovacs");
        }
    }
}
