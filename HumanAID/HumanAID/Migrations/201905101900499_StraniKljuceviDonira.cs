namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StraniKljuceviDonira : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Doniras",
                c => new
                    {
                        IdSponzora = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdSponzora, t.IdDogadjaja })
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja, cascadeDelete: true)
                .ForeignKey("dbo.Sponzors", t => t.IdSponzora, cascadeDelete: true)
                .Index(t => t.IdSponzora)
                .Index(t => t.IdDogadjaja);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Doniras", "IdSponzora", "dbo.Sponzors");
            DropForeignKey("dbo.Doniras", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.Doniras", new[] { "IdDogadjaja" });
            DropIndex("dbo.Doniras", new[] { "IdSponzora" });
            DropTable("dbo.Doniras");
        }
    }
}
