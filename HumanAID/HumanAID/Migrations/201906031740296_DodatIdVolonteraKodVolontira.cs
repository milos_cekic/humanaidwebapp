namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatIdVolonteraKodVolontira : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Volontiras", "IdVolontera", c => c.String(maxLength: 128));
            CreateIndex("dbo.Volontiras", "IdVolontera");
            AddForeignKey("dbo.Volontiras", "IdVolontera", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Volontiras", "IdVolontera", "dbo.AspNetUsers");
            DropIndex("dbo.Volontiras", new[] { "IdVolontera" });
            DropColumn("dbo.Volontiras", "IdVolontera");
        }
    }
}
