namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SredjenDogadjajIIdKaUseru : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "IdKaOrganizaciji", c => c.String(maxLength: 128));
            CreateIndex("dbo.Dogadjajs", "IdKaOrganizaciji");
            AddForeignKey("dbo.Dogadjajs", "IdKaOrganizaciji", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dogadjajs", "IdKaOrganizaciji", "dbo.AspNetUsers");
            DropIndex("dbo.Dogadjajs", new[] { "IdKaOrganizaciji" });
            DropColumn("dbo.Dogadjajs", "IdKaOrganizaciji");
        }
    }
}
