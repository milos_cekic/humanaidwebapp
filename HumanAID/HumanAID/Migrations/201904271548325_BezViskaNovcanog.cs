namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezViskaNovcanog : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Dogadjajs", "VisakNovcano");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dogadjajs", "VisakNovcano", c => c.Double(nullable: false));
        }
    }
}
