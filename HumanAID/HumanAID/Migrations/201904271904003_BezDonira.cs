namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezDonira : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs");
            DropForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors");
            DropIndex("dbo.Doniras", new[] { "Dogadjaj_Id" });
            DropIndex("dbo.Doniras", new[] { "Sponzor_Id" });
            DropTable("dbo.Doniras");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Doniras",
                c => new
                    {
                        IdSponzora = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                        Dogadjaj_Id = c.Int(),
                        Sponzor_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.IdSponzora, t.IdDogadjaja });
            
            CreateIndex("dbo.Doniras", "Sponzor_Id");
            CreateIndex("dbo.Doniras", "Dogadjaj_Id");
            AddForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors", "Id");
            AddForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs", "Id");
        }
    }
}
