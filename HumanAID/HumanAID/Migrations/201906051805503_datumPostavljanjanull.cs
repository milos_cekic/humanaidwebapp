namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datumPostavljanjanull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Dogadjajs", "DatumPostavljanja", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dogadjajs", "DatumPostavljanja", c => c.DateTime(nullable: false));
        }
    }
}
