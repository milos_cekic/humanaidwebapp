namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrikupljenaRoba : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PrikupljenaRobas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivPrikupljeneRobe = c.String(nullable: false),
                        DoniranaTip = c.Int(nullable: false),
                        PrikupioRobu = c.String(nullable: false),
                        Kolicina = c.Int(nullable: false),
                        IdPotrebneRobe = c.Int(nullable: false),
                        IdSponzora = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja, cascadeDelete: false)
                .ForeignKey("dbo.Robas", t => t.IdPotrebneRobe, cascadeDelete: false)
                .ForeignKey("dbo.Sponzors", t => t.IdSponzora, cascadeDelete: true)
                .Index(t => t.IdPotrebneRobe)
                .Index(t => t.IdSponzora)
                .Index(t => t.IdDogadjaja);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors");
            DropForeignKey("dbo.PrikupljenaRobas", "IdPotrebneRobe", "dbo.Robas");
            DropForeignKey("dbo.PrikupljenaRobas", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdDogadjaja" });
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdSponzora" });
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdPotrebneRobe" });
            DropTable("dbo.PrikupljenaRobas");
        }
    }
}
