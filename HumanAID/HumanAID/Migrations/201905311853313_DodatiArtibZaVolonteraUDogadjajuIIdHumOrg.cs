namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatiArtibZaVolonteraUDogadjajuIIdHumOrg : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "BrojPotrebnihVolontera", c => c.Int());
            AddColumn("dbo.Dogadjajs", "BrojTrenutnihVolontera", c => c.Int());
            CreateIndex("dbo.Dogadjajs", "IdOrganizacije");
            AddForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas", "Id", cascadeDelete: true);
            DropColumn("dbo.Dogadjajs", "MinimalniBudzet");
            DropColumn("dbo.Dogadjajs", "MinimalniBudzetPrikupljeno");
            DropColumn("dbo.Dogadjajs", "MinimalniBudzetPreostalo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dogadjajs", "MinimalniBudzetPreostalo", c => c.Double());
            AddColumn("dbo.Dogadjajs", "MinimalniBudzetPrikupljeno", c => c.Double());
            AddColumn("dbo.Dogadjajs", "MinimalniBudzet", c => c.Double());
            DropForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas");
            DropIndex("dbo.Dogadjajs", new[] { "IdOrganizacije" });
            DropColumn("dbo.Dogadjajs", "BrojTrenutnihVolontera");
            DropColumn("dbo.Dogadjajs", "BrojPotrebnihVolontera");
        }
    }
}
