namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdSponzoraUStringKodDonira : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Doniras", "IdSponzora", c => c.String(maxLength: 128));
            CreateIndex("dbo.Doniras", "IdSponzora");
            AddForeignKey("dbo.Doniras", "IdSponzora", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Doniras", "IdSponzora", "dbo.AspNetUsers");
            DropIndex("dbo.Doniras", new[] { "IdSponzora" });
            DropColumn("dbo.Doniras", "IdSponzora");
        }
    }
}
