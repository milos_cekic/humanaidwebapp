namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezIdSponzoraKodDonira : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Doniras", "IdSponzora", "dbo.Sponzors");
            DropIndex("dbo.Doniras", new[] { "IdSponzora" });
            DropColumn("dbo.Doniras", "IdSponzora");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Doniras", "IdSponzora", c => c.Int(nullable: false));
            CreateIndex("dbo.Doniras", "IdSponzora");
            AddForeignKey("dbo.Doniras", "IdSponzora", "dbo.Sponzors", "Id", cascadeDelete: true);
        }
    }
}
