// <auto-generated />
namespace HumanAID.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class IdOrganizacijeNull : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(IdOrganizacijeNull));
        
        string IMigrationMetadata.Id
        {
            get { return "201906031531270_IdOrganizacijeNull"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
