namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatIdDogadjajaKodPrikupljeneRobe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrikupljenaRobas", "IdDogadjaja", c => c.Int(nullable: false));
            CreateIndex("dbo.PrikupljenaRobas", "IdDogadjaja");
            AddForeignKey("dbo.PrikupljenaRobas", "IdDogadjaja", "dbo.Dogadjajs", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdDogadjaja" });
            DropColumn("dbo.PrikupljenaRobas", "IdDogadjaja");
        }
    }
}
