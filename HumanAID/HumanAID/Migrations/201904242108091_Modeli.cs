namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modeli : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Administrators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Sifra = c.String(),
                        Email = c.String(),
                        Kontakt = c.String(),
                        Adresa = c.String(),
                        Grad = c.String(),
                        Opis = c.String(),
                        LinkKaSajtu = c.String(),
                        LogoPutanja = c.String(),
                        DatumRegistracijeKorisnika = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dogadjajs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivDogadjaja = c.String(),
                        MestoOdrzavanja = c.String(),
                        DatumPocetkaDogadjaja = c.DateTime(nullable: false),
                        DatumKrajaDogadjaja = c.DateTime(nullable: false),
                        MinimalniBudzet = c.Double(nullable: false),
                        MinimalniBudzetPrikupljeno = c.Double(nullable: false),
                        MinimalniBudzetPreostalo = c.Double(nullable: false),
                        VisakNovcano = c.Double(nullable: false),
                        ZiroRacun = c.String(),
                        Opis = c.String(),
                        Faza = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HumanitarnaOrganizacijas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivOrganizacije = c.String(),
                        DatumOsnivanjaOrganizacije = c.DateTime(nullable: false),
                        GlavniRacun = c.String(),
                        Username = c.String(),
                        Sifra = c.String(),
                        Email = c.String(),
                        Kontakt = c.String(),
                        Adresa = c.String(),
                        Grad = c.String(),
                        Opis = c.String(),
                        LinkKaSajtu = c.String(),
                        LogoPutanja = c.String(),
                        DatumRegistracijeKorisnika = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Sponzors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(),
                        DatumOsnivanja = c.DateTime(nullable: false),
                        Delatnost = c.String(),
                        Username = c.String(),
                        Sifra = c.String(),
                        Email = c.String(),
                        Kontakt = c.String(),
                        Adresa = c.String(),
                        Grad = c.String(),
                        Opis = c.String(),
                        LinkKaSajtu = c.String(),
                        LogoPutanja = c.String(),
                        DatumRegistracijeKorisnika = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Volonters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ime = c.String(),
                        Prezime = c.String(),
                        DatumRodjenja = c.DateTime(nullable: false),
                        Pol = c.String(),
                        BrojBedzeva = c.Int(nullable: false),
                        Status = c.String(),
                        Username = c.String(),
                        Sifra = c.String(),
                        Email = c.String(),
                        Kontakt = c.String(),
                        Adresa = c.String(),
                        Grad = c.String(),
                        Opis = c.String(),
                        LinkKaSajtu = c.String(),
                        LogoPutanja = c.String(),
                        DatumRegistracijeKorisnika = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.Volonters");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Sponzors");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.HumanitarnaOrganizacijas");
            DropTable("dbo.Dogadjajs");
            DropTable("dbo.Administrators");
        }
    }
}
