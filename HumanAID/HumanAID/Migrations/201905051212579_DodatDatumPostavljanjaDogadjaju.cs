namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatDatumPostavljanjaDogadjaju : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dogadjajs", "DatumPostavljanja", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dogadjajs", "DatumPostavljanja");
        }
    }
}
