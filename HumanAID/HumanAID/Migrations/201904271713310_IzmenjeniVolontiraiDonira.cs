namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IzmenjeniVolontiraiDonira : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Doniras",
                c => new
                {
                    IdSponzora = c.Int(nullable: false),
                    IdDogadjaja = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.IdSponzora, t.IdDogadjaja })
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja)
                .ForeignKey("dbo.Sponzors", t => t.IdSponzora)
                .Index(t => t.IdDogadjaja)
                .Index(t => t.IdSponzora);

            CreateTable(
                "dbo.Volontiras",
                c => new
                {
                    IdVolontera = c.Int(nullable: false),
                    IdDogadjaja = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.IdVolontera, t.IdDogadjaja })
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja)
                .ForeignKey("dbo.Volonters", t => t.IdVolontera)
                .Index(t => t.IdDogadjaja)
                .Index(t => t.IdVolontera);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Volontiras", "IdVolontera", "dbo.Volonters");
            DropForeignKey("dbo.Volontiras", "IdDogadjaja", "dbo.Dogadjajs");
            DropForeignKey("dbo.Doniras", "IdSponzora", "dbo.Sponzors");
            DropForeignKey("dbo.Doniras", "IdDogadjaja", "dbo.Dogadjajs");
            DropIndex("dbo.Volontiras", new[] { "IdVolontera" });
            DropIndex("dbo.Volontiras", new[] { "IdDogadjaja" });
            DropIndex("dbo.Doniras", new[] { "IdSponzora" });
            DropIndex("dbo.Doniras", new[] { "IdDogadjaja" });
            DropTable("dbo.Volontiras");
            DropTable("dbo.Doniras");
        }
    }
}
