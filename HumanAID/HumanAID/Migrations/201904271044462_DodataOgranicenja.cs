namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodataOgranicenja : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Administrators", "Username", c => c.String(nullable: false, maxLength: 35));
            AlterColumn("dbo.Administrators", "Email", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Administrators", "Kontakt", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Dogadjajs", "NazivDogadjaja", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Dogadjajs", "Opis", c => c.String(nullable: false));
            AlterColumn("dbo.HumanitarnaOrganizacijas", "NazivOrganizacije", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.HumanitarnaOrganizacijas", "GlavniRacun", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.HumanitarnaOrganizacijas", "Username", c => c.String(nullable: false, maxLength: 35));
            AlterColumn("dbo.HumanitarnaOrganizacijas", "Email", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.HumanitarnaOrganizacijas", "Kontakt", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Sponzors", "Naziv", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Sponzors", "Delatnost", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Sponzors", "Username", c => c.String(nullable: false, maxLength: 35));
            AlterColumn("dbo.Sponzors", "Email", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Sponzors", "Kontakt", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Volonters", "Ime", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.Volonters", "Prezime", c => c.String(nullable: false, maxLength: 35));
            AlterColumn("dbo.Volonters", "Username", c => c.String(nullable: false, maxLength: 35));
            AlterColumn("dbo.Volonters", "Email", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Volonters", "Kontakt", c => c.String(nullable: false, maxLength: 255));
            DropColumn("dbo.Administrators", "Sifra");
            DropColumn("dbo.HumanitarnaOrganizacijas", "Sifra");
            DropColumn("dbo.Sponzors", "Sifra");
            DropColumn("dbo.Volonters", "Sifra");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Volonters", "Sifra", c => c.String());
            AddColumn("dbo.Sponzors", "Sifra", c => c.String());
            AddColumn("dbo.HumanitarnaOrganizacijas", "Sifra", c => c.String());
            AddColumn("dbo.Administrators", "Sifra", c => c.String());
            AlterColumn("dbo.Volonters", "Kontakt", c => c.String());
            AlterColumn("dbo.Volonters", "Email", c => c.String());
            AlterColumn("dbo.Volonters", "Username", c => c.String());
            AlterColumn("dbo.Volonters", "Prezime", c => c.String());
            AlterColumn("dbo.Volonters", "Ime", c => c.String());
            AlterColumn("dbo.Sponzors", "Kontakt", c => c.String());
            AlterColumn("dbo.Sponzors", "Email", c => c.String());
            AlterColumn("dbo.Sponzors", "Username", c => c.String());
            AlterColumn("dbo.Sponzors", "Delatnost", c => c.String());
            AlterColumn("dbo.Sponzors", "Naziv", c => c.String());
            AlterColumn("dbo.HumanitarnaOrganizacijas", "Kontakt", c => c.String());
            AlterColumn("dbo.HumanitarnaOrganizacijas", "Email", c => c.String());
            AlterColumn("dbo.HumanitarnaOrganizacijas", "Username", c => c.String());
            AlterColumn("dbo.HumanitarnaOrganizacijas", "GlavniRacun", c => c.String());
            AlterColumn("dbo.HumanitarnaOrganizacijas", "NazivOrganizacije", c => c.String());
            AlterColumn("dbo.Dogadjajs", "Opis", c => c.String());
            AlterColumn("dbo.Dogadjajs", "NazivDogadjaja", c => c.String());
            AlterColumn("dbo.Administrators", "Kontakt", c => c.String());
            AlterColumn("dbo.Administrators", "Email", c => c.String());
            AlterColumn("dbo.Administrators", "Username", c => c.String());
        }
    }
}
