namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SklonjeniKomentariUDonira : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Doniras", "Dogadjaj_Id", c => c.Int());
            AddColumn("dbo.Doniras", "Sponzor_Id", c => c.Int());
            CreateIndex("dbo.Doniras", "Dogadjaj_Id");
            CreateIndex("dbo.Doniras", "Sponzor_Id");
            AddForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs", "Id");
            AddForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors");
            DropForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs");
            DropIndex("dbo.Doniras", new[] { "Sponzor_Id" });
            DropIndex("dbo.Doniras", new[] { "Dogadjaj_Id" });
            DropColumn("dbo.Doniras", "Sponzor_Id");
            DropColumn("dbo.Doniras", "Dogadjaj_Id");
        }
    }
}
