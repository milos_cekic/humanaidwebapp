// <auto-generated />
namespace HumanAID.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DodatSurogatKljucKodVolontira : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DodatSurogatKljucKodVolontira));
        
        string IMigrationMetadata.Id
        {
            get { return "201905292001136_DodatSurogatKljucKodVolontira"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
