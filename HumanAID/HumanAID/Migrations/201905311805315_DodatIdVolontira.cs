namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatIdVolontira : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Volontiras");
            AddColumn("dbo.Volontiras", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Volontiras", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Volontiras");
            DropColumn("dbo.Volontiras", "Id");
            AddPrimaryKey("dbo.Volontiras", new[] { "IdVolontera", "IdDogadjaja" });
        }
    }
}
