namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatIdSponzoraKodPrikupljene : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrikupljenaRobas", "IdSponzora", c => c.Int(nullable: false));
            CreateIndex("dbo.PrikupljenaRobas", "IdSponzora");
            AddForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdSponzora" });
            DropColumn("dbo.PrikupljenaRobas", "IdSponzora");
        }
    }
}
