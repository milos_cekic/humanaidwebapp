namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezFaze : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dogadjajs", "FazaId", "dbo.FazaDogadjajas");
            DropIndex("dbo.Dogadjajs", new[] { "FazaId" });
            AddColumn("dbo.Dogadjajs", "Faza", c => c.Int(nullable: false));
            DropTable("dbo.FazaDogadjajas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FazaDogadjajas",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Prosli = c.String(),
                        Aktuelni = c.String(),
                        UPripremi = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Dogadjajs", "Faza");
            CreateIndex("dbo.Dogadjajs", "FazaId");
            AddForeignKey("dbo.Dogadjajs", "FazaId", "dbo.FazaDogadjajas", "Id", cascadeDelete: true);
        }
    }
}
