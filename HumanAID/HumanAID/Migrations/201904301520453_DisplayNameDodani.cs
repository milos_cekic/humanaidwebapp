namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DisplayNameDodani : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Volonters", "BrojBedzeva", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Volonters", "BrojBedzeva", c => c.Int(nullable: false));
        }
    }
}
