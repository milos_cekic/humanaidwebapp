namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezIdOrganizacije : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas");
            DropIndex("dbo.Dogadjajs", new[] { "IdOrganizacije" });
            DropColumn("dbo.Dogadjajs", "IdOrganizacije");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dogadjajs", "IdOrganizacije", c => c.Int(nullable: false));
            CreateIndex("dbo.Dogadjajs", "IdOrganizacije");
            AddForeignKey("dbo.Dogadjajs", "IdOrganizacije", "dbo.HumanitarnaOrganizacijas", "Id", cascadeDelete: true);
        }
    }
}
