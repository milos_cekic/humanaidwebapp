// <auto-generated />
namespace HumanAID.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DodatIdVolontira : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DodatIdVolontira));
        
        string IMigrationMetadata.Id
        {
            get { return "201905311805315_DodatIdVolontira"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
