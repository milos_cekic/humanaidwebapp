namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrenutnoPrikupljenoKodRObe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Robas", "PrikupljenaRoba", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Robas", "PrikupljenaRoba");
        }
    }
}
