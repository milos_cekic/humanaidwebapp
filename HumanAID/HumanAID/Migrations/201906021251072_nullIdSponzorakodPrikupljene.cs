namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullIdSponzorakodPrikupljene : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdSponzora" });
            AlterColumn("dbo.PrikupljenaRobas", "IdSponzora", c => c.Int());
            CreateIndex("dbo.PrikupljenaRobas", "IdSponzora");
            AddForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdSponzora" });
            AlterColumn("dbo.PrikupljenaRobas", "IdSponzora", c => c.Int(nullable: false));
            CreateIndex("dbo.PrikupljenaRobas", "IdSponzora");
            AddForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors", "Id", cascadeDelete: true);
        }
    }
}
