namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullablePotrebnaKolicinaNovca : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PotrebanNovacs", "PotrebnaKolicinaNovca", c => c.Single());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PotrebanNovacs", "PotrebnaKolicinaNovca", c => c.Single(nullable: false));
        }
    }
}
