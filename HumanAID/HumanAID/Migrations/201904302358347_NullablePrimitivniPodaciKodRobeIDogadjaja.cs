namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullablePrimitivniPodaciKodRobeIDogadjaja : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Dogadjajs", "MinimalniBudzet", c => c.Double());
            AlterColumn("dbo.Dogadjajs", "MinimalniBudzetPrikupljeno", c => c.Double());
            AlterColumn("dbo.Dogadjajs", "MinimalniBudzetPreostalo", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dogadjajs", "MinimalniBudzetPreostalo", c => c.Double(nullable: false));
            AlterColumn("dbo.Dogadjajs", "MinimalniBudzetPrikupljeno", c => c.Double(nullable: false));
            AlterColumn("dbo.Dogadjajs", "MinimalniBudzet", c => c.Double(nullable: false));
        }
    }
}
