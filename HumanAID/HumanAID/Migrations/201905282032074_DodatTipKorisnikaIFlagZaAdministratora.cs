namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatTipKorisnikaIFlagZaAdministratora : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "KorisnikTip", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Administrator", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Administrator");
            DropColumn("dbo.AspNetUsers", "KorisnikTip");
        }
    }
}
