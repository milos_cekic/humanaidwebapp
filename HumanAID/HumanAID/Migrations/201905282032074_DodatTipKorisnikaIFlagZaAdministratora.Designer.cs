// <auto-generated />
namespace HumanAID.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DodatTipKorisnikaIFlagZaAdministratora : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DodatTipKorisnikaIFlagZaAdministratora));
        
        string IMigrationMetadata.Id
        {
            get { return "201905282032074_DodatTipKorisnikaIFlagZaAdministratora"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
