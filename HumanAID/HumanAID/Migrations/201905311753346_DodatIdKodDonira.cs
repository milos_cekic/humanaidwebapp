namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatIdKodDonira : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Doniras");
            AddColumn("dbo.Doniras", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Doniras", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Doniras");
            DropColumn("dbo.Doniras", "Id");
            AddPrimaryKey("dbo.Doniras", new[] { "IdSponzora", "IdDogadjaja" });
        }
    }
}
