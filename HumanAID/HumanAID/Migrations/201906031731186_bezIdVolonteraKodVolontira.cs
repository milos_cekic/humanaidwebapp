namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bezIdVolonteraKodVolontira : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Volontiras", "IdVolontera", "dbo.Volonters");
            DropIndex("dbo.Volontiras", new[] { "IdVolontera" });
            DropColumn("dbo.Volontiras", "IdVolontera");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Volontiras", "IdVolontera", c => c.Int(nullable: false));
            CreateIndex("dbo.Volontiras", "IdVolontera");
            AddForeignKey("dbo.Volontiras", "IdVolontera", "dbo.Volonters", "Id", cascadeDelete: true);
        }
    }
}
