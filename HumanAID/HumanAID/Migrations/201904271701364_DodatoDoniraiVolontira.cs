namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatoDoniraiVolontira : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Doniras",
                c => new
                    {
                        IdSponzora = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdSponzora, t.IdDogadjaja })
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja)
                .ForeignKey("dbo.Sponzors", t => t.IdSponzora)
                .Index(t => t.IdDogadjaja)
                .Index(t => t.IdSponzora);
            
            CreateTable(
                "dbo.Volontiras",
                c => new
                    {
                        IdVolontera = c.Int(nullable: false),
                        IdDogadjaja = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdVolontera, t.IdDogadjaja })
                .ForeignKey("dbo.Dogadjajs", t => t.IdDogadjaja)
                .ForeignKey("dbo.Volonters", t => t.IdVolontera)
                .Index(t => t.IdDogadjaja)
                .Index(t => t.IdVolontera);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Volontiras", "Volonter_Id", "dbo.Volonters");
            DropForeignKey("dbo.Volontiras", "Dogadjaj_Id", "dbo.Dogadjajs");
            DropForeignKey("dbo.Doniras", "Sponzor_Id", "dbo.Sponzors");
            DropForeignKey("dbo.Doniras", "Dogadjaj_Id", "dbo.Dogadjajs");
            DropIndex("dbo.Volontiras", new[] { "Volonter_Id" });
            DropIndex("dbo.Volontiras", new[] { "Dogadjaj_Id" });
            DropIndex("dbo.Doniras", new[] { "Sponzor_Id" });
            DropIndex("dbo.Doniras", new[] { "Dogadjaj_Id" });
            DropTable("dbo.Volontiras");
            DropTable("dbo.Doniras");
        }
    }
}
