namespace HumanAID.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BezIdSpoznoraKodPrikupljeneRobe : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors");
            DropIndex("dbo.PrikupljenaRobas", new[] { "IdSponzora" });
            DropColumn("dbo.PrikupljenaRobas", "IdSponzora");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PrikupljenaRobas", "IdSponzora", c => c.Int());
            CreateIndex("dbo.PrikupljenaRobas", "IdSponzora");
            AddForeignKey("dbo.PrikupljenaRobas", "IdSponzora", "dbo.Sponzors", "Id");
        }
    }
}
