﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using HumanAID.Models;
using HumanAID.Dtos;

namespace HumanAID.App_Start
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Volonter, VolonterDto>();
            Mapper.CreateMap<VolonterDto, Volonter>();
            Mapper.CreateMap<Sponzor, SponzorDto>();
            Mapper.CreateMap<SponzorDto, Sponzor>();
            Mapper.CreateMap<HumanitarnaOrganizacija, HumanitarnaOrganizacijaDto>();
            Mapper.CreateMap<HumanitarnaOrganizacijaDto, HumanitarnaOrganizacija>();
            Mapper.CreateMap<Dogadjaj, DogadjajDto>();
            Mapper.CreateMap<DogadjajDto, Dogadjaj>();
            Mapper.CreateMap<Donira, DoniraDto>();
            Mapper.CreateMap<DoniraDto, Donira>();
            Mapper.CreateMap<Volontira, VolontiraDto>();
            Mapper.CreateMap<VolontiraDto, Volontira>();

            Mapper.CreateMap <IEnumerable<Dogadjaj>, IEnumerable<DogadjajDto>>();
            Mapper.CreateMap<IEnumerable<DogadjajDto>, IEnumerable<Dogadjaj>>();
            //mislim da ovo nece da nam treba nego ce nam trebati Ienumerable lista volontira i donira
            Mapper.CreateMap<IEnumerable<Donira>, IEnumerable<DoniraDto>>();
            Mapper.CreateMap<IEnumerable<DoniraDto>, IEnumerable<Donira>>();

            Mapper.CreateMap<IEnumerable<Volontira>, IEnumerable<VolontiraDto>>();
            Mapper.CreateMap<IEnumerable<VolontiraDto>, IEnumerable<Volontira>>();


            Mapper.CreateMap<Roba, RobaDto>();
            Mapper.CreateMap<RobaDto, Roba>();

            Mapper.CreateMap<PrikupljenaRoba, PrikupljenaRobaDto>();
            Mapper.CreateMap<PrikupljenaRobaDto, PrikupljenaRoba>();
            Mapper.CreateMap<ApplicationUser, UserDto>();
            Mapper.CreateMap<UserDto, ApplicationUser>();

            Mapper.CreateMap<PotrebanNovac, PotrebanNovacDto>();
            Mapper.CreateMap<PotrebanNovacDto, PotrebanNovac>();

            Mapper.CreateMap<NovcaneDonacije, NovcaneDonacijeDto>();
            Mapper.CreateMap<NovcaneDonacijeDto, NovcaneDonacije>();

        }
    }
}