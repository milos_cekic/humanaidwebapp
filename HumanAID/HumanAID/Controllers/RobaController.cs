﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HumanAID.Models;
using HumanAID.Dtos;

namespace HumanAID.Controllers
{
    public class RobaController : Controller
    {
        private ApplicationDbContext _context;
        public RobaController()
        {
            _context = new ApplicationDbContext();
        }
        //mora i dispose
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Roba
        public ActionResult Index()
        {
            return View();
        }
        [Route("roba/novaroba")]
        public ActionResult NoviProizvod()
        {
            return View("RobaForm");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("roba/sacuvajrobu")]
        public ActionResult SacuvajRobu(Roba roba)
        {
            if (!ModelState.IsValid)
            {
                return View("RobaForm", roba);
            }

            if (roba.Id == 0)
            {
                roba.PrikupljenaRoba = 0;
                _context.Roba.Add(roba);


                //vraticu se ovde, poenta je da se recimo potrebanNovac kreira prilikomm kreiranja dogadjaja
                //pa makar on bio null jer ovde mozemo da dodajemo recimo 5. robu sto znaci da treba da nadjemo taj potrebanNOvac
                //i da povecamo kolicinu ili smanjimo potrebnog novca
                var potrebanNovacInDb = _context.PotrebanNovac.SingleOrDefault(p => p.IdDogadjaja == roba.DogadjajId);
                potrebanNovacInDb.PotrebnaKolicinaNovca += roba.UkupnaCenaProizvoda();
            }
            else
            {
                var robaInDb = _context.Roba.Single(r => r.Id == roba.Id);

                robaInDb.Proizvod = roba.Proizvod;

                var potrebanNovacInDb = _context.PotrebanNovac.SingleOrDefault(p => p.IdDogadjaja == roba.DogadjajId);
                if (robaInDb.PotrebnaKolicina < roba.PotrebnaKolicina)
                    potrebanNovacInDb.PotrebnaKolicinaNovca += (roba.UkupnaCenaProizvoda() - robaInDb.UkupnaCenaProizvoda());
                else
                    potrebanNovacInDb.PotrebnaKolicinaNovca -= (robaInDb.UkupnaCenaProizvoda() - roba.UkupnaCenaProizvoda());

                //dalje nastvaljamo
                robaInDb.PotrebnaKolicina = roba.PotrebnaKolicina;
                //misli se na trenutnu kolicinu
                var listaPrikupljeneRobeZaPotrebnu = _context.PrikupljenaRoba.Where(p => p.IdPotrebneRobe == robaInDb.Id);
                int trenutna = 0;
                foreach (PrikupljenaRoba prikupljena in listaPrikupljeneRobeZaPotrebnu)
                    trenutna += prikupljena.Kolicina;

                robaInDb.PrikupljenaRoba = trenutna;
                robaInDb.CenaProizvodaPoKomadu = roba.CenaProizvodaPoKomadu;
                robaInDb.DogadjajId = roba.DogadjajId;
                
            }

            _context.SaveChanges();
            //sada je problem sa index stranom, tj. ne redirectuje kako treba
            //ali upisuje u bazu kako treba
            return RedirectToAction("Index", "Roba");

        }

        [Route("roba/izmeni/{id}")]
        public ActionResult Izmeni(int id)
        {
            var roba = _context.Roba.SingleOrDefault(r => r.Id == id);
            if (roba == null)
                return HttpNotFound();

            return View("RobaForm", roba);
        }


    }
}