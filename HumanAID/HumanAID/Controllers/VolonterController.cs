﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HumanAID.Models;
using HumanAID.ViewModels;
using Microsoft.AspNet.Identity;
using System.IO;

namespace HumanAID.Controllers
{
    public class VolonterController : Controller
    {
        private ApplicationDbContext _context;
        public VolonterController()
        {
            _context = new ApplicationDbContext();
        }
        //mora i dispose
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Volonter
        [Route("volonter/index")]
        public ActionResult Index()
        {
            var viewModel = new SviVolonteriViewModel
            {
                ListaVolontera = _context.Users.Where(u => u.KorisnikTip == TipKorisnika.Volonter).ToList()
            };

            string userID = User.Identity.GetUserId();
            if (userID != null)
            {
                //korisnik je ulogovan i proveravamo da li je admin
                var user = _context.Users.Single(u => u.Id == userID);
                if (user.Administrator)

                    return View("SviVolonteriAdmin", viewModel);
            }

            return View(viewModel);
        }

        public ActionResult NoviVolonter()
        {
            return View("VolonterForm");
        }
        
        [Route("volonter/izmeni/{id}")]
        public ActionResult Izmeni(string id)
        {
            var volonter = _context.Users.SingleOrDefault(o => o.Id == id);
            if (volonter == null)
                return HttpNotFound();

            return View("VolonterForm", volonter);
        }

        [HttpPost]
        [Route("volonter/novaslika")]
        public ActionResult SlikaNova(HttpPostedFileBase file, string id)
        {

            string putanja = "";
            string idVolontera = id;
            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/images/"), fileName);
                file.SaveAs(path);
                // sada kada ubacimo sliku treba da sacuvamo tu sliku
                putanja = fileName;
            }
            //ar userID = User.Identity.GetUserId();
            var volonter = _context.Users.SingleOrDefault(d => d.Id == idVolontera);
            if (volonter != null)
            {
                volonter.LogoPutanja = putanja;
                _context.SaveChanges();

            }
            if (volonter != null)
            {
                return RedirectToAction("PrikaziProfilVolontera", "Volonter", new { id = volonter.Id, vidljivost = true });
            }

            return RedirectToAction("PrikaziPoFazi", "Dogadjaj", new { faza = FazaDogadjaja.Aktuelni });
        }


        [Route("volonter/profil/{id}/{vidljivost}")]
        public ActionResult PrikaziProfilVolontera(string id, bool vidljivost)
        {
            var volonter = _context.Users.SingleOrDefault(c => c.Id == id);

            if (volonter == null)
                return HttpNotFound();


            if (vidljivost)
            {
                var viewModel = new ProfilVolonteraViewModel
                {
                    Volonter = volonter,
                    ListaVolontira = _context.Volontira
                    .Where(m => m.IdVolontera == id).ToList()
                };

                return View("ProfilVolonteraSvoj", viewModel);
            }
            else
            {

                var viewModel = new ProfilVolonteraViewModel
                {
                    Volonter = volonter,
                    ListaVolontira = _context.Volontira
                    .Where(m => m.IdVolontera == id).ToList()
                };
                return View("ProfilVolonteraOstali", viewModel);
            }
        }
    }
}