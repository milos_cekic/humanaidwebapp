﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HumanAID.Models;
using HumanAID.ViewModels;
using System.Collections;
using Microsoft.AspNet.Identity;
using System.IO;

namespace HumanAID.Controllers
{
    public class SponzorController : Controller
    {

            private ApplicationDbContext _context;
            public SponzorController()
            {
                _context = new ApplicationDbContext();
            }
            //mora i dispose
            protected override void Dispose(bool disposing)
            {
                _context.Dispose();
            }

            // GET: Sponzor
            public ActionResult Index()
            {
                var viewModel = new SviSponzoriViewModel
                {
                    ListaSponzora = _context.Users.Where(u => u.KorisnikTip == TipKorisnika.Sponzor).ToList()
                };

                string userID = User.Identity.GetUserId();
                if (userID != null)
                {
                    //korisnik je ulogovan i proveravamo da li je admin
                    var user = _context.Users.Single(u => u.Id == userID);
                    if (user.Administrator)

                        return View("SviSponzoriAdmin", viewModel);
                }

                return View(viewModel);
             }


            [Route("sponzor/novisponzor")]
            public ActionResult NoviSponzor()
            {
                return View("SponzorForm");
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            [Route("sponzor/sacuvajsponzora")]
            public ActionResult SacuvajSponzora(Sponzor sponzor)
            {
                if (!ModelState.IsValid)
                {
                    return View("SponzorForm", sponzor);
                }


                if (sponzor.Id == 0)
                {
                    //to je novi sponzor
                    //ovo ovde trenutno mora ovako dok ne sredimo konacnu formu za registraciju
                    sponzor.DatumRegistracijeKorisnika = DateTime.Now;
                    _context.Sponzori.Add(sponzor);
                }
                else
                {
                    var sponzorInDb = _context.Sponzori.Single(s => s.Id == sponzor.Id);
                    //kasnije cemo da napravimo mozda UpdateSponzoraDTO zbog sigurnosnih propusta mappera
                    //Mapper.Map da probamo kasnije
                    sponzorInDb.Email = sponzor.Email;
                    sponzorInDb.Username = sponzor.Username;
                    sponzorInDb.Naziv = sponzor.Naziv;
                    sponzorInDb.DatumOsnivanja = sponzor.DatumOsnivanja;
                    sponzorInDb.Delatnost = sponzor.Delatnost;
                    sponzorInDb.Kontakt = sponzor.Kontakt;
                    sponzorInDb.Adresa = sponzor.Adresa;
                    sponzorInDb.Grad = sponzor.Grad;
                    sponzorInDb.Opis = sponzor.Opis;
                    sponzorInDb.LinkKaSajtu = sponzor.LinkKaSajtu;
                    sponzorInDb.LogoPutanja = sponzor.LogoPutanja;

                }
                _context.SaveChanges();
                return RedirectToAction("Index", "Sponzor");

            }

            [Route("sponzor/izmeni/{id}")]
            public ActionResult Izmeni(string id)
            {
                var sponzor = _context.Users.SingleOrDefault(o => o.Id == id);
                if (sponzor == null)
                    return HttpNotFound();

                return View("SponzorForm", sponzor);
            }


        [HttpPost]
        [Route("sponzor/novaslika")]
        public ActionResult SlikaNova(HttpPostedFileBase file, string id)
        {


            string putanja = "";
            string idOrganizacije = id;
            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/images/"), fileName);
                file.SaveAs(path);
                // sada kada ubacimo sliku treba da sacuvamo tu sliku
                putanja = fileName;
            }
            //ar userID = User.Identity.GetUserId();
            var sponzor = _context.Users.SingleOrDefault(d => d.Id == idOrganizacije);
            if (sponzor != null)
            {
                sponzor.LogoPutanja = putanja;
                _context.SaveChanges();

            }
            if (sponzor != null)
            {
                return RedirectToAction("PrikaziProfilSponzora", "Sponzor", new { id = sponzor.Id, vidljivost = true });
            }

            return RedirectToAction("PrikaziPoFazi", "Dogadjaj", new { faza = FazaDogadjaja.Aktuelni });
        }


        [Route("sponzor/profil/{id}/{vidljivost}")]
            public ActionResult PrikaziProfilSponzora(string id, bool vidljivost)
            {
                var sponzor = _context.Users.SingleOrDefault(c => c.Id == id);

                if (sponzor == null)
                    return HttpNotFound();
                
                if(vidljivost)
                {
                    var viewModel = new ProfilSponzoraViewModel
                    {
                        Sponzor = sponzor,
                        ListaDonira = _context.Donira
                        .Where(m => m.IdSponzora == id).ToList()
                    };

                    return View("ProfilSponzoraSvoj",viewModel);
                }
                else
                {

                var viewModel = new ProfilSponzoraViewModel
                {
                    Sponzor = sponzor,
                    ListaDonira = _context.Donira
                        .Where(m => m.IdSponzora == id).ToList()
                };

                return View("ProfilSponzoraOstali",viewModel);
            }
        
            }
        }
    }
