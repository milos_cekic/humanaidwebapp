﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HumanAID.Models;
using HumanAID.ViewModels;
using Microsoft.AspNet.Identity;
using HumanAID.Dtos;
using System.IO;
using AutoMapper;
using System.Net.Mail;
using System.Net;

namespace HumanAID.Controllers
{
    
    public class DogadjajController : Controller
    {
        public decimal IzracunajProgres(int id)
        {
            decimal ukupnaKolicinaPotrebneRobe = 0;
            decimal rez = 0;
            decimal count = 0;
            var listaPotrebneRobe = _context.Roba
             .Where(r => r.DogadjajId == id).ToList();

            if (listaPotrebneRobe != null)
            {
                foreach (var roba in listaPotrebneRobe)
                {
                    //nadjemo ukupnu potrebnu kolicinu ove jedne potrebne robe
                    //mozda fali ovo roba.PotrebnaKolicina!=0
                    if (roba.PotrebnaKolicina != null || roba.PotrebnaKolicina != 0)
                    {
                        ukupnaKolicinaPotrebneRobe += ((decimal)roba.PrikupljenaRoba / (decimal)roba.PotrebnaKolicina);
                        count++;
                    }
                }

                //zbog deljenja sa nulom
                if (count == 0)
                    rez = 0;
                else
                    rez = ukupnaKolicinaPotrebneRobe / count;
            }
          

            //kada smo odradili za robu sada mozemo i za volontere ako ima potrebe da se radi za volontere
            var dogadjaj = _context.Dogadjaji.SingleOrDefault(c => c.Id == id);
            if (dogadjaj.BrojPotrebnihVolontera!=null || dogadjaj.BrojPotrebnihVolontera!=0)
            {
                rez += (decimal)((decimal)dogadjaj.BrojTrenutnihVolontera /(decimal) dogadjaj.BrojPotrebnihVolontera);
                rez /= 2;
                if (rez > 1)
                    return rez = 1;
                else
                return rez;
            }
            else
            {
                //ili mogu da vracam 1 kao 100% ili 100
                if (rez > 1)
                    return rez = 1;
                else
                return rez;
            }

        }

        private ApplicationDbContext _context;
        public DogadjajController()
        {
            _context = new ApplicationDbContext();
        }
        //mora i dispose
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Dogadjaj
        //index ce kod nas da bude aktuelni jer nam je to glavni cilj da se vide aktuelni dogadjaji

        [Route("dogadjaj/slika")]
        public ActionResult SlikaView()
        {
            return View("DogadjajAjaxPost");
        }

        [HttpPost]
        [Route("dogadjaj/novaslika")]
        public ActionResult SlikaNova(HttpPostedFileBase file, int id)
        {
            string putanja = "";
            int idDogadjaja = id;
            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/images/"), fileName);
                file.SaveAs(path);
                // sada kada ubacimo sliku treba da sacuvamo tu sliku
                putanja = fileName;
            }
            //ar userID = User.Identity.GetUserId();
            var dogadjaj = _context.Dogadjaji.SingleOrDefault(d => d.Id == idDogadjaja);
            if (dogadjaj != null)
            {
                dogadjaj.UrlSlike = putanja;
                _context.SaveChanges();

            }
            if (dogadjaj != null)
            {
                return RedirectToAction("PrikazDogadjaja", "Dogadjaj", new { id = dogadjaj.Id});
            }

            return RedirectToAction("PrikaziPoFazi", "Dogadjaj", new { faza = FazaDogadjaja.Aktuelni });
        }


        //[Route("dogadjaj/novaslika")]
        //[HttpPost]
        //public ActionResult NovaSlika(HttpPostedFileBase file)
        //{

        //    if (file.ContentLength > 0)
        //    {
        //        var fileName = Path.GetFileName(file.FileName);
        //        var path = Path.Combine(Server.MapPath("~/App_Data"), fileName);
        //        file.SaveAs(path);
        //    }

        //    return RedirectToAction("NoviDogadjaj");
        //}

        [Route("dogadjaj/{faza}")]
        public ActionResult PrikaziPoFazi(FazaDogadjaja faza)
        {
            switch (faza)
            {
                case FazaDogadjaja.Prosli:
                    {
                        //recimo 6 sati nakon zavrsetka, preglednosti radi
                        DateTime danasnji = DateTime.Now.AddHours(6);

                        var listaAktuelnihProvera = _context.Dogadjaji.Where(d => d.Faza == FazaDogadjaja.Aktuelni).ToList();
                            foreach(var aktuelni in listaAktuelnihProvera)
                            {
                                DateTime datumKraja = aktuelni.DatumKrajaDogadjaja;
                                int result = DateTime.Compare(datumKraja, danasnji);
                                if (result < 0)
                                    aktuelni.Faza = FazaDogadjaja.Prosli;
                            _context.SaveChanges();
                                //proveravamo da li je neki od aktuelnih dogadjaja ostao kao aktuelan iako se zavrsio
                                //(taj slucaj je jedino moguc ako je dogadjaj prosao i niko nije usao na stranici dogadjaja
                            };

                        var listaProslih = _context.Dogadjaji.Where(d => d.Faza == faza).ToList();


                        List<decimal> procenti = new List<decimal>(listaProslih.Count());
                        decimal procenat = 1;
                        foreach(var dogadjaj in listaProslih)
                           procenti.Add(procenat);
                        //ovde bi trebalo da postoji ona logika za dogadjaje koji su aktuelni a prosao im je datum izvrsenja
                        //da prodju u fazu prosli                        
                        var viewModel = new ProsliDogadjajiViewModel
                        {

                            ListaProslihDogadjaja = listaProslih,
                            ProcenatIspunjenosti = procenti
                        };


                        return View("ProsliDogadjaji", viewModel);
                    }
                case FazaDogadjaja.UPripremi:
                    {
 
                        var listaUPripremi = _context.Dogadjaji.Where(d => d.Faza == faza).ToList();

                        List<decimal> procenti = new List<decimal>(listaUPripremi.Count());

                        decimal procenat = 0;
                        foreach (var dogadjaj in listaUPripremi)
                        {
                            procenat = IzracunajProgres(dogadjaj.Id);
                            procenti.Add(procenat);
                        }

                        var viewModel = new DogadjajiUPripremiViewModel
                        {
                            //ovo da proverim da li je dobar _context
                            //prvo zbog faze, a potom i zbog toList()
                            ListaDogadjajaUPripremi = listaUPripremi,
                            ProcenatIspunjenosti = procenti
                        };


                        return View("DogadjajiUPripremi", viewModel);
                    }
                case FazaDogadjaja.Aktuelni:
                    {
                        var listaAktuelnih = _context.Dogadjaji.Where(d => d.Faza == faza).ToList();

                        List<decimal> procenti = new List<decimal>(listaAktuelnih.Count());
                        DateTime danasnji = DateTime.Now.AddHours(6);

                        decimal procenat = 1;
                        foreach (var dogadjaj in listaAktuelnih)
                        {

                            DateTime datumKraja = dogadjaj.DatumKrajaDogadjaja;
                            int result = DateTime.Compare(datumKraja, danasnji);
                            if (result < 0)
                            {
                                dogadjaj.Faza = FazaDogadjaja.Prosli;
                                _context.SaveChanges();

                                //proveravamo da li je neki od aktuelnih dogadjaja ostao kao aktuelan iako se zavrsio
                                //(taj slucaj je jedino moguc ako je dogadjaj prosao i niko nije usao na stranici dogadjaja
                            }
                            else
                            {

                                procenti.Add(procenat);
                            }
                        }
                        var viewModel = new AktuelniDogadjajiViewModel
                        {
                            ListaAktuelnihDogadjaja = _context.Dogadjaji.Where(d => d.Faza == faza).ToList(),
                            ProcenatIspunjenosti=procenti
                        };


                        return View("AktuelniDogadjaji", viewModel);
                    }
                default:
                    {
                        var listaAktuelnih = _context.Dogadjaji.Where(d => d.Faza == faza).ToList();

                        List<decimal> procenti = new List<decimal>(listaAktuelnih.Count());

                        decimal procenat = 1;
                        foreach (var dogadjaj in listaAktuelnih)
                            procenti.Add(procenat);


                        var viewModel = new AktuelniDogadjajiViewModel
                        {
                            ListaAktuelnihDogadjaja = _context.Dogadjaji.Where(d => d.Faza == faza).ToList(),
                            ProcenatIspunjenosti = procenti
                        };


                        return View("AktuelniDogadjaji", viewModel);
                    }
            }

        }
        [Route("dogadjaj/prikazdogadjaja/{id}")]
        public ActionResult PrikazDogadjaja(int id)
        {
            var dogadjaj = _context.Dogadjaji.SingleOrDefault(c => c.Id == id);
            if (dogadjaj == null)
                return HttpNotFound("Ne postoji trazeni dogadjaj.");

            String idOrg = dogadjaj.IdKaOrganizaciji;
            var organizacija = _context.Users.SingleOrDefault(c => c.Id == idOrg);
            if (organizacija == null)
                return HttpNotFound("Dogadjaj nije kreirala ni jedna organizacija.");

            //recimo 6 sati nakon zavrsetka, preglednosti radi
            DateTime datumKraja = dogadjaj.DatumKrajaDogadjaja;
            DateTime danasnji = DateTime.Now.AddHours(6);
            int result = DateTime.Compare(datumKraja, danasnji);
            if (result < 0)
                dogadjaj.Faza = FazaDogadjaja.Prosli;

            _context.SaveChanges();
            if (dogadjaj.Faza == FazaDogadjaja.Aktuelni)
            {
                    var viewModelAktuelni = new PrikazDogadjajaViewModel
                    {
                        Dogadjaj = dogadjaj,
                        Organizacija = organizacija,

                        ListaPotrebneRobe = null,
                        ListaPrikupljeneRobe = null,

                        ListaDonira = _context.Donira
                            .Where(m => m.IdDogadjaja == id).ToList(),

                        ListaVolontira = _context.Volontira
                        .Where(m => m.IdDogadjaja == id).ToList(),

                        IdTrenutnogUlogovanogKorisnika = null,
                        IdVolontira = 0,
                        IdDonira = 0,
                        ProcenatIspunjenosti=1,
                        ListaPrikupljeneRobeUlogovanogSponzora=null,
                        ListaPrikupljenogNovcaUlogovanogSponzora=null,
                        PotrebanNovac= _context.PotrebanNovac.SingleOrDefault(n => n.Id==dogadjaj.IdPotrebnogNovca)
            };

                return View("PrikazAktuelnogDogadjajaSvi", viewModelAktuelni);
            }
            else if(dogadjaj.Faza==FazaDogadjaja.Prosli)
            {
                var viewModelProsli = new PrikazDogadjajaViewModel
                {
                    Dogadjaj = dogadjaj,
                    Organizacija = organizacija,

                    ListaPotrebneRobe = null,
                    ListaPrikupljeneRobe = null,

                    ListaDonira = _context.Donira
                            .Where(m => m.IdDogadjaja == id).ToList(),

                    ListaVolontira = _context.Volontira
                        .Where(m => m.IdDogadjaja == id).ToList(),

                    IdTrenutnogUlogovanogKorisnika = null,
                    IdVolontira = 0,
                    IdDonira = 0,
                    ProcenatIspunjenosti=1,
                    ListaPrikupljeneRobeUlogovanogSponzora = null,
                    ListaPrikupljenogNovcaUlogovanogSponzora = null,
                    PotrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == dogadjaj.IdPotrebnogNovca)
                };

                //neka za sada bude ovako videcemo sta ce sve da se salje, ovo bi bio default aktuelni i to 
                return View("PrikazProslogDogadjajaSvi", viewModelProsli);
            }
            else
            {
                //do ovde sada dolazimo bez pitanja da li je korisnik logovan, ovde je vec bitno da li je korisnik
                //logovan i kako i kakva je veza sa njim i dogadjajem
                string userID = User.Identity.GetUserId();
                if (userID != null)
                {
                    //korisnik je ulogovan pa treba da nadjemo koji je to korsinik zbog tipa korisnika
                    var user = _context.Users.Single(u => u.Id == userID);
                    if (user.KorisnikTip == TipKorisnika.Volonter)
                    {
                        var volontira = _context.Volontira.SingleOrDefault(v => v.IdDogadjaja == id && v.IdVolontera == userID);
                        int idVolontira = 0;

                        if (volontira == null)
                            idVolontira = 0;
                        else
                            idVolontira = volontira.Id;
                        //ako je volontira null i dosli smo do ovde, to znaci da je trenutni user
                        //volonter i da ne volontira na ovom dogadjaju, ako ne onda volontira na dogadjaju

                        var viewModelVolonter = new PrikazDogadjajaViewModel
                        {
                            Dogadjaj = dogadjaj,
                            Organizacija = organizacija,

                            ListaPotrebneRobe = _context.Roba
                             .Where(r => r.DogadjajId == id),

                            //lista prikupljene nam je jedino bitna zbog sponzora
                            ListaPrikupljeneRobe = null,

                            ListaDonira = _context.Donira
                            .Where(m => m.IdDogadjaja == id).ToList(),

                            ListaVolontira = _context.Volontira
                        .Where(m => m.IdDogadjaja == id).ToList(),

                            IdTrenutnogUlogovanogKorisnika = userID,
                            IdVolontira = idVolontira,
                            IdDonira = 0,
                            ProcenatIspunjenosti = IzracunajProgres(dogadjaj.Id),
                            ListaPrikupljeneRobeUlogovanogSponzora = null,
                            ListaPrikupljenogNovcaUlogovanogSponzora = null,
                            PotrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == dogadjaj.IdPotrebnogNovca)
                        };

                        return View("PrikazDogadjajUPripremiZaVolontera", viewModelVolonter);
                    }
                    else if (user.KorisnikTip == TipKorisnika.Sponzor)
                    {
                        var donira = _context.Donira.SingleOrDefault(d => d.IdDogadjaja == id && d.IdSponzora == userID);
                        int idDonira = 0;

                        if (donira == null)
                            idDonira = 0;
                        else
                            idDonira = donira.Id;
                        //ako je donira null i dosli smo do ovde, to znaci da je trenutni user
                        //sponor i da ne sponzorise ovaj dogadjaju, ako ne onda sponzorise dogadjaj

                        var viewModelSponzor = new PrikazDogadjajaViewModel
                        {
                            Dogadjaj = dogadjaj,
                            Organizacija = organizacija,

                            ListaPotrebneRobe = _context.Roba
                             .Where(r => r.DogadjajId == id),

                            //lista prikupljene nam je jedino bitna zbog sponzora
                            ListaPrikupljeneRobe = _context.PrikupljenaRoba
                         .Where(r => r.IdDogadjaja == id),

                            ListaDonira = _context.Donira
                            .Where(m => m.IdDogadjaja == id).ToList(),

                            ListaVolontira = _context.Volontira
                        .Where(m => m.IdDogadjaja == id).ToList(),

                            IdTrenutnogUlogovanogKorisnika = userID,
                            IdVolontira = 0,
                            IdDonira = idDonira,
                            ProcenatIspunjenosti = IzracunajProgres(dogadjaj.Id),
                            ListaPrikupljeneRobeUlogovanogSponzora = _context.PrikupljenaRoba
                         .Where(r => r.IdDogadjaja == id && r.IdMogucegSponzora==userID),
                            ListaPrikupljenogNovcaUlogovanogSponzora = _context.NovcaneDonacije
                         .Where(r => r.IdDogadjaja == id && r.IdSponzora==userID),
                            PotrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == dogadjaj.IdPotrebnogNovca)
                        };

                        return View("PrikazDogadjajUPripremiZaSponzora", viewModelSponzor);

                    }
                    else
                    {
                        //onda je sigurno organizacija i potrebno je proveriti da li je ona napravila dogadjaj
                        //ili je korisnik tipa administrator koji ima pristup svemu
                        if(user.Id==dogadjaj.IdKaOrganizaciji || user.Administrator==true)
                        {
                            var viewModelOrganizacija = new PrikazDogadjajaViewModel
                            {
                                Dogadjaj = dogadjaj,
                                Organizacija = organizacija,

                                ListaPotrebneRobe = _context.Roba
                                 .Where(r => r.DogadjajId == id),

                                //lista prikupljene nam je jedino bitna zbog sponzora
                                ListaPrikupljeneRobe = _context.PrikupljenaRoba
                             .Where(r => r.IdDogadjaja == id),

                                ListaDonira = _context.Donira
                            .Where(m => m.IdDogadjaja == id).ToList(),

                                ListaVolontira = _context.Volontira
                        .Where(m => m.IdDogadjaja == id).ToList(),

                                IdTrenutnogUlogovanogKorisnika = userID,
                                IdVolontira = 0,
                                IdDonira = 0,
                                ProcenatIspunjenosti = IzracunajProgres(dogadjaj.Id),
                                ListaPrikupljeneRobeUlogovanogSponzora = null,
                                ListaPrikupljenogNovcaUlogovanogSponzora = null,
                                PotrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == dogadjaj.IdPotrebnogNovca)
                            };

                            return View("PrikazDogadjajUPripremiZaOrganizaciju", viewModelOrganizacija);

                        }
                        else
                        {
                            //prikaz je kao i korisnik koji nije ulogovan
                            var viewModelUPripremiObican = new PrikazDogadjajaViewModel
                            {
                                Dogadjaj = dogadjaj,
                                Organizacija = organizacija,

                                ListaPotrebneRobe = _context.Roba
                                     .Where(r => r.DogadjajId == id),

                                 ListaPrikupljeneRobe = null,

                                ListaDonira = _context.Donira
                            .Where(m => m.IdDogadjaja == id).ToList(),

                                ListaVolontira = _context.Volontira
                        .Where(m => m.IdDogadjaja == id).ToList(),

                                IdTrenutnogUlogovanogKorisnika = userID,
                                IdVolontira = 0,
                                IdDonira = 0,
                                ProcenatIspunjenosti = IzracunajProgres(dogadjaj.Id),
                                ListaPrikupljeneRobeUlogovanogSponzora = null,
                                ListaPrikupljenogNovcaUlogovanogSponzora = null,
                                PotrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == dogadjaj.IdPotrebnogNovca)
                            };

                            return View("PrikazDogadjajaUPripremiObican", viewModelUPripremiObican);
                        }
                    }
                    
                }
                else
                {

                    //korisnik nije ulogovan i poseduje poseban prikaz dogadjaja koji je u pripremi
                    var viewModelUPripremiObican = new PrikazDogadjajaViewModel
                    {
                        Dogadjaj = dogadjaj,
                        Organizacija = organizacija,

                        ListaPotrebneRobe = _context.Roba
                             .Where(r => r.DogadjajId == id),

                        ListaPrikupljeneRobe = null,

                        ListaDonira = _context.Donira
                            .Where(m => m.IdDogadjaja == id).ToList(),

                        ListaVolontira = _context.Volontira
                        .Where(m => m.IdDogadjaja == id).ToList(),

                        IdTrenutnogUlogovanogKorisnika = null,
                        IdVolontira = 0,
                        IdDonira = 0
                    };
                    return View("PrikazDogadjajaUPripremiObican", viewModelUPripremiObican);
                }
            }
            
        }
        [Authorize]
        [Route("dogadjaj/kreiraj")]
        public ActionResult KreirajDogadjaj()
        {
            var userId = User.Identity.GetUserId();
            var user = _context.Users.SingleOrDefault(u => u.Id == userId);
            if (user.KorisnikTip == TipKorisnika.Organizacija)
            {

                return View("DogadjajNovi");
            }
            else
                return RedirectToAction("PrikaziPoFazi", "Dogadjaj", new { faza = FazaDogadjaja.Aktuelni });
        }

        [Authorize]
        [Route("dogadjaj/izmeni/{id}")]
        public ActionResult IzmeniDogadjaj(int id)
        {
            var dogadjaj = _context.Dogadjaji.SingleOrDefault(d => d.Id == id);
            if (dogadjaj == null)
                return HttpNotFound();


            var userId = User.Identity.GetUserId();

            if (dogadjaj.IdKaOrganizaciji == userId)
            {


                return View("DogadjajIzmeni", dogadjaj);
            }
            else
                return RedirectToAction("PrikaziPoFazi", "Dogadjaj", new { faza = FazaDogadjaja.Aktuelni });
        }

        //hteli smo ovo ali je bilo problema, pa ce da ostane za kasnije ili neku drugu verziju
        //[HttpPost]
        //public ActionResult SendEmail(string receiver, string subject, string message)
        //{

        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var senderEmail = new MailAddress("ahumanaid@gmail.com", "Admin");
        //            var receiverEmail = new MailAddress(receiver, "Receiver");
        //            var password = "HumanaidAdmin@1";
        //            var sub = subject;
        //            var body = message;
        //            var smtp = new SmtpClient
        //            {
        //                Host = "smtp.gmail.com",
        //                Port = 587,
        //                EnableSsl = true,
        //                DeliveryMethod = SmtpDeliveryMethod.Network,
        //                UseDefaultCredentials = false,
        //                Credentials = new NetworkCredential(senderEmail.Address, password)
        //            };
        //            using (var mess = new MailMessage(senderEmail, receiverEmail)
        //            {
        //                Subject = subject,
        //                Body = body
        //            })
        //            {
        //                smtp.Send(mess);
        //            }
        //            return View("DogadjajForm");
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        ViewBag.Error = "Doslo je do greske prilikom slanja mejla.";
        //    }
        //    return View();
        //}
    }
}