﻿using AutoMapper;
using HumanAID.Dtos;
using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace HumanAID.Controllers.Api
{
    public class Dogadjaji2Controller : ApiController
    {
        private ApplicationDbContext _context;
        public Dogadjaji2Controller()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult NoviDogadjaj(DogadjajDto dogadjajDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            dogadjajDto.DatumPostavljanja = DateTime.Now;

            if(dogadjajDto.UploadImage!=null)
            {
                string fileName = Path.GetFileNameWithoutExtension(dogadjajDto.UploadImage.FileName);
                string extension = Path.GetExtension(dogadjajDto.UploadImage.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
                dogadjajDto.UrlSlike = fileName;
                dogadjajDto.UploadImage.SaveAs(Path.Combine("~/App_Data/", fileName));
            }



            var dogadjaj = Mapper.Map<DogadjajDto, Dogadjaj>(dogadjajDto);
            _context.Dogadjaji.Add(dogadjaj);
            dogadjajDto.Id = dogadjaj.Id;

            //prvo kreiramo dogadjaj bez idPotrebogNovca

            PotrebanNovac potrebanNovac = new PotrebanNovac()
            {
                PotrebnaKolicinaNovca = 0,
                TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje = 0,
                UkupnoPrikupljenaKolicinaNovca = 0,
                IdDogadjaja = dogadjaj.Id
            };

            _context.PotrebanNovac.Add(potrebanNovac);
            //ponovo vracamo zbog povezanosti
            dogadjaj.IdPotrebnogNovca = potrebanNovac.Id;

            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + dogadjaj.Id), dogadjajDto);

        }
    }
}
