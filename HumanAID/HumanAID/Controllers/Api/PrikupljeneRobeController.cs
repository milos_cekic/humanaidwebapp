﻿using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HumanAID.Dtos;
using AutoMapper;
using Microsoft.AspNet.Identity;

namespace HumanAID.Controllers.Api
{
    public class PrikupljeneRobeController : ApiController
    {
        private ApplicationDbContext _context;

        public PrikupljeneRobeController()
        {
            _context = new ApplicationDbContext();
        }
        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}
        //GET /api/prikupljenerobe
        [HttpGet]
        public IEnumerable<PrikupljenaRobaDto> VratiPrikupljeneRobe()
        {
            return _context.PrikupljenaRoba.ToList().Select(Mapper.Map<PrikupljenaRoba, PrikupljenaRobaDto>);
        }
        //GET /api/prikupljenerobe/1
        [HttpGet]
        public IHttpActionResult VratiPrikupljenuRobu(int id)
        {
            var roba = _context.PrikupljenaRoba.SingleOrDefault(r => r.Id == id);

            if (roba == null)
                return NotFound();

            return Ok(Mapper.Map<PrikupljenaRoba, PrikupljenaRobaDto>(roba));
        }

        // POST /api/prikupljenerobe
        [HttpPost]
        public IHttpActionResult NovaPrikupljena(PrikupljenaRobaDto robaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //promenimo prvo stanje u Potrebnoj robi jer se izvrsila nova donacija
            //i ka vezama PotrebanNovac i NovcaneDonacije ako ima potrebe
            //i tu donaciju upisemo u prikupljenoj robji

                
                //ovde idemo logikom da ukoliko je birano da donacija bude robnog tipa
                //mora da postoji idMoguceg sponzora
                //ukoliko nekim cudom ne postoji bice potreban jos jedan if iznad
                if (robaDto.IdMogucegSponzora != null)
                {
                    //to znaci da je donirao spoznor, a ne org prikupljenim sredstvima
                    //sada proveravamo da li postoji zapis o sponzoru i dogadjaju u Donira
                    var donira = _context.Donira.SingleOrDefault(d => d.IdDogadjaja == robaDto.IdDogadjaja && d.IdSponzora == robaDto.IdMogucegSponzora);
                    if (donira == null)
                    {
                        Donira doniraAdd = new Donira
                        {
                            IdDogadjaja = robaDto.IdDogadjaja,
                            IdSponzora = User.Identity.GetUserId()
                        };
                        _context.Donira.Add(doniraAdd);
                    _context.SaveChanges();
                    }
                }
                
            var potrebnaRoba = _context.Roba.SingleOrDefault(r => r.Id == robaDto.IdPotrebneRobe);

            if (potrebnaRoba == null)
                return BadRequest("Ne postoji roba za doniranje.");


            potrebnaRoba.PrikupljenaRoba += robaDto.Kolicina;
            
            _context.SaveChanges();
            //sada treba iz potrebnog novca da smanjimo potreban novac za onoliko koliko je donirano*cenaPoKomadu
            var novac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == potrebnaRoba.IdPotrebnogNovca);
            if (novac != null)
            {
                if (novac.TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje > 0)
                {
                    //proveriti da li je int ili decimal rezultat
                    novac.PotrebnaKolicinaNovca -= robaDto.Kolicina * potrebnaRoba.CenaProizvodaPoKomadu;
                    //do ovde se izvrsi nevezano da li je org ili sponzor

                    //ukoliko je org kupila robu doniranim novcem novcanih sponzora
                    //postoji jos jedan dodatni korak zbog one funkcije da li trenutnim stanjem novca
                    //koje org poseduje moze da kupi sve ostale potrebstine
                    if (robaDto.DoniranaTip == TipDoniranja.PrikupljenimNovcem)
                        novac.TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje -= robaDto.Kolicina * potrebnaRoba.CenaProizvodaPoKomadu;
                }
                else
                    return BadRequest("Nemate dovoljno sredstava da kupite robu.");
            }
            //ako je novac null, obrisan je iz nekog razloga, ali to ne utice
            //na dalji tok aplikacije

            var roba = Mapper.Map<PrikupljenaRobaDto, PrikupljenaRoba>(robaDto);
            _context.PrikupljenaRoba.Add(roba);
            //proveriti da li se ovaj SaveChanges odnosi na sve objekte koji su povuceni iz baze
            _context.SaveChanges();

            robaDto.Id = roba.Id;
            return Created(new Uri(Request.RequestUri + "/" + roba.Id), robaDto);

        }
        //put smo hteli da napravimo ali smo ustanovili da mozda nema smisla jer bi to bila njegova poslednja donacija, ne bi bile sve donacije
        //ili ukupna kolicina koju je on donirao, sponzor i dalje moze da donira pojedinacno po odredjenu svotu donacija

        ////PUT /api/prikupljenerobe/1
        //[HttpPut]
        //public IHttpActionResult AzurirajPrikupljenuRobu(int id, PrikupljenaRobaDto robaDto)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest();

        //    var robaInDb = _context.PrikupljenaRoba.SingleOrDefault(r => r.Id == id);
        //    //treba da ispitamo da li je sponozr/org smanjio u odnosu na prethodnu donaciju ili povecao
        //    if(robaInDb.Kolicina<)


        //    if (robaInDb == null)
        //        return NotFound();

        //    Mapper.Map(robaDto, robaInDb);

        //    _context.SaveChanges();
        //    return Ok();

        //}

        //brisanje konkretne prikupljeneRobne donacije, ovo ce se pozivati 
        //tako sto sponzor selektuje brisanje celokupnih donacija na tom dogadjaju
        //i obrisu se sve donacije putem ajaxa i sledece metode
        //DELETE /api/prikupljenerobe/1
        [HttpDelete]
        public IHttpActionResult ObrisiPrikupljenuRobu(int id)
        {
            //trebalo bi da obrise prikupljenu robu i da smanji za tu kolicinu iz Potrebne robe onoliko koliko je prikupljeno
            //i zatim da se to promeni u potrebnoj kolicini novca takodje
            var robaInDb = _context.PrikupljenaRoba.SingleOrDefault(r => r.Id == id);

            if (robaInDb == null)
                return NotFound();

            //prvo smanjujemo iz potrebne kolicine
            var potrebnaRoba = _context.Roba.SingleOrDefault(r => r.Id == robaInDb.IdPotrebneRobe);
            if (potrebnaRoba != null)
               potrebnaRoba.PrikupljenaRoba -= robaInDb.Kolicina;
            //ako ta roba postoji smanji joj se vrednost    

            //potrebno je jos i da se poveca kolicina Potrebnog Novca jer je brisanje prikupljene robe
            //uticalo na potrebnu robu, a bilo kakva izmena nad potrebnom robom utice i na novac  
            var potrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == potrebnaRoba.IdPotrebnogNovca);
            if (potrebanNovac != null)
            {
                //proveriti da li je int ili decimal rezultat
                potrebanNovac.PotrebnaKolicinaNovca += robaInDb.Kolicina * potrebnaRoba.CenaProizvodaPoKomadu;
            }

            
            _context.PrikupljenaRoba.Remove(robaInDb);
            _context.SaveChanges();

            //zatim logika da li da se obrise i iz Donira
            var listaPrikupljenihRobaSponzora = _context.PrikupljenaRoba.Where(r => r.IdMogucegSponzora == robaInDb.IdMogucegSponzora).ToList();
            var listaDonacijaZaSponzora = _context.NovcaneDonacije.Where(d => d.IdSponzora == robaInDb.IdMogucegSponzora).ToList();
            if (listaPrikupljenihRobaSponzora == null && listaDonacijaZaSponzora == null)
            {
                var donira = _context.Donira.SingleOrDefault(d => d.IdDogadjaja == robaInDb.IdDogadjaja && d.IdSponzora == robaInDb.IdMogucegSponzora);
                if (donira != null)
                {
                    _context.Donira.Remove(donira);
                    _context.SaveChanges();
                }
            }


            return Ok();
        }

    }
}
