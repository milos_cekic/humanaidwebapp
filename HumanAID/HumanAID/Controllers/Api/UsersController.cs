﻿using AutoMapper;
using HumanAID.Dtos;
using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HumanAID.Controllers.Api
{
    public class UsersController : ApiController
    {
        private ApplicationDbContext _context;
        public UsersController()
        {
            _context = new ApplicationDbContext();
        }
        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}
        //GET /api/humanitarneorganizacije
        //[HttpGet]
        //public IEnumerable<HumanitarnaOrganizacijaDto> VratiOrganizaije()
        //{
        //    return _context.HumanitarneOrganizacije.ToList().Select(Mapper.Map<HumanitarnaOrganizacija, HumanitarnaOrganizacijaDto>);
        //}
        //GET /api/users/1
        [HttpGet]
        public IHttpActionResult VratiKorisnika(string id)
        {
            var user = _context.Users.SingleOrDefault(o => o.Id == id);

            if (user == null)
                return NotFound();

            return Ok(Mapper.Map<ApplicationUser, UserDto>(user));
        }

        //PUT /api/users/1
        [HttpPut]
        public IHttpActionResult AzurirajKorisnika(string id, UserDto userDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var userInDb = _context.Users.SingleOrDefault(o => o.Id == id);

            if (userInDb == null)
                return NotFound();

            Mapper.Map(userDto, userInDb);

            _context.SaveChanges();
            return Ok();

        }
        //DELETE /api/users/1
        [HttpDelete]
        public IHttpActionResult ObrisiKorisnika(string id)
        {
            var userInDb = _context.Users.SingleOrDefault(o => o.Id == id);

            if (userInDb == null)
                return NotFound();


            if (userInDb.KorisnikTip == TipKorisnika.Volonter)
            {

                var volontira = _context.Volontira.Where(v => v.IdVolontera == id).ToList();
                if (volontira != null)
                {
                    foreach(var v in volontira)
                    {
                        _context.Volontira.Remove(v);
                        _context.SaveChanges();
                    }
                }
            }
            else if(userInDb.KorisnikTip == TipKorisnika.Organizacija)
            {
                var dogadjaji = _context.Dogadjaji.Where(v => v.IdKaOrganizaciji == id).ToList();
                if (dogadjaji != null)
                {
                    foreach (var dogadjaj in dogadjaji)
                    {
                        _context.Dogadjaji.Remove(dogadjaj);
                        _context.SaveChanges();
                    }
                }
            }
            else
            {
                var donira = _context.Donira.Where(v => v.IdSponzora == id).ToList();
                if (donira != null)
                {
                    foreach (var d in donira)
                    {
                        _context.Donira.Remove(d);
                        _context.SaveChanges();
                    }
                }
                var prikupljeneRobe = _context.PrikupljenaRoba.Where(v => v.IdMogucegSponzora == id).ToList();
                if (prikupljeneRobe != null)
                {
                    foreach (var prikupljena in prikupljeneRobe)
                    {
                        _context.PrikupljenaRoba.Remove(prikupljena);
                        _context.SaveChanges();
                    }
                }


                var donacije = _context.NovcaneDonacije.Where(v => v.IdSponzora == id).ToList();
                if (donacije != null)
                {
                    foreach (var d in donacije)
                    {
                        _context.NovcaneDonacije.Remove(d);
                        _context.SaveChanges();
                    }
                }
                
            }
            _context.Users.Remove(userInDb);
            _context.SaveChanges();
            return Ok();

        }

    }
}
