﻿using AutoMapper;
using HumanAID.Dtos;
using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HumanAID.Controllers.Api
{
    public class RobeController : ApiController
    {
        private ApplicationDbContext _context;
        public RobeController()
        {
            _context = new ApplicationDbContext();
        }
        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}
        //GET /api/robe
        [HttpGet]
        public IEnumerable<RobaDto> VratiProizvode()
        {
            return _context.Roba.ToList().Select(Mapper.Map<Roba, RobaDto>);
        }
        //GET /api/robe/1
        [HttpGet]
        public IHttpActionResult VratiProizvod(int id)
        {
            var roba = _context.Roba.SingleOrDefault(r => r.Id == id);

            if (roba == null)
                return NotFound();

            return Ok(Mapper.Map<Roba, RobaDto>(roba));
        }

        // POST /api/robe
        [HttpPost]
        public IHttpActionResult NoviProizvod(RobaDto robaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            //bitno je da zapamtimo da potrebnoj robi dodelimo id potrebnog novca
            //koji cemo da nadjemo preko id dogadjaja jer i on ima referencu na potreban novac
            
            var roba = Mapper.Map<RobaDto, Roba>(robaDto);
            _context.Roba.Add(roba);
            _context.SaveChanges();
            //kada dodamo potrebnu robu treba da uticemo i na potreban novac
            var potrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == robaDto.IdPotrebnogNovca);
            if (potrebanNovac != null)
            {
                //proveriti da li je int ili decimal rezultat
                potrebanNovac.PotrebnaKolicinaNovca += roba.UkupnaCenaProizvoda();
            }

            _context.SaveChanges();
            robaDto.Id = roba.Id;
            return Created(new Uri(Request.RequestUri + "/" + roba.Id), robaDto);

        }

        //PUT /api/robe/1
        [HttpPut]
        public IHttpActionResult AzurirajProizvod(int id, RobaDto robaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //prvo nadjemo staru potrebnu robu
            var robaInDb = _context.Roba.SingleOrDefault(r => r.Id == id);

            if (robaInDb == null)
                return NotFound();

            //zatim uticemo na potreban novac, proveravamo da li moramo da ga smanjimo
            //ili povecamo
            var potrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == robaDto.IdPotrebnogNovca);
            if (potrebanNovac != null)
            {
                decimal novaKolUkupno = (decimal)(robaDto.PotrebnaKolicina * robaDto.CenaProizvodaPoKomadu);
                if (robaInDb.UkupnaCenaProizvoda() < novaKolUkupno)
                    potrebanNovac.PotrebnaKolicinaNovca += (novaKolUkupno - robaInDb.UkupnaCenaProizvoda());
                 else
                    potrebanNovac.PotrebnaKolicinaNovca -= (robaInDb.UkupnaCenaProizvoda()- novaKolUkupno);

                //moramo da povecamo ili smanjimo za razliku u novcu prilikom izmene
            }

            Mapper.Map(robaDto, robaInDb);

            _context.SaveChanges();
            return Ok();

        }
        //DELETE /api/robe/1
        [HttpDelete]
        public IHttpActionResult ObrisiProizvod(int id)
        {
            var robaInDb = _context.Roba.SingleOrDefault(r => r.Id == id);

            if (robaInDb == null)
                return NotFound();

            //kada brisemo potrebnu robu treba da uticemo i na potreban novac
            var potrebanNovac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == robaInDb.IdPotrebnogNovca);
            if (potrebanNovac != null)
            {
                //proveriti da li je int ili decimal rezultat
                potrebanNovac.PotrebnaKolicinaNovca -= robaInDb.UkupnaCenaProizvoda();
            }

            var prikupljenaRoba = _context.PrikupljenaRoba.Where(p => p.IdPotrebneRobe == id).ToList();
            if(prikupljenaRoba!=null)
            {

                foreach(var roba in prikupljenaRoba)
                {
                    _context.PrikupljenaRoba.Remove(roba);
                    _context.SaveChanges();
                }
            }
            
            _context.Roba.Remove(robaInDb);
            _context.SaveChanges();
            return Ok();

        }

    }
}
