﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HumanAID.Dtos;
using AutoMapper;
using HumanAID.Models;
using Microsoft.AspNet.Identity;

namespace HumanAID.Controllers.Api
{
    public class DogadjajiController : ApiController
    {
        private ApplicationDbContext _context;
        public DogadjajiController()
        {
            _context = new ApplicationDbContext();
        }
        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}
        //GET /api/dogadjaji
        [HttpGet]
        public IEnumerable<DogadjajDto> VratiDogadjaje()
        {
            return _context.Dogadjaji.ToList().Select(Mapper.Map<Dogadjaj, DogadjajDto>);
        }
        //GET /api/dogadjaji/1
        [HttpGet]
        public IHttpActionResult VratiDogadjaj(int id)
        {
            var dogadjaj = _context.Dogadjaji.SingleOrDefault(d => d.Id == id);

            if (dogadjaj == null)
                return NotFound();

            return Ok(Mapper.Map<Dogadjaj, DogadjajDto>(dogadjaj));
        }

        // POST /api/dogadjaji
        [HttpPost]
        public IHttpActionResult NoviDogadjaj(DogadjajDto dogadjajDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            dogadjajDto.DatumPostavljanja = DateTime.Now;

            var dogadjaj = Mapper.Map<DogadjajDto, Dogadjaj>(dogadjajDto);
            _context.Dogadjaji.Add(dogadjaj);
            dogadjajDto.Id = dogadjaj.Id;

            //prvo kreiramo dogadjaj bez idPotrebogNovca

            PotrebanNovac potrebanNovac = new PotrebanNovac()
            {
                PotrebnaKolicinaNovca = 0,
                TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje = 0,
                UkupnoPrikupljenaKolicinaNovca = 0,
                IdDogadjaja=dogadjaj.Id
            };

            _context.PotrebanNovac.Add(potrebanNovac);
            _context.SaveChanges();
            //ponovo vracamo zbog povezanosti
            dogadjaj.IdPotrebnogNovca = potrebanNovac.Id;

            _context.SaveChanges();

           return Created(new Uri(Request.RequestUri + "/" + dogadjaj.Id), dogadjaj);
        }

        //PUT /api/dogadjaji/1
        [HttpPut]
        public IHttpActionResult AzurirajDogadjaj(int id, DogadjajDto dogadjajDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var dogadjajInDb = _context.Dogadjaji.SingleOrDefault(d => d.Id == id);

            if (dogadjajInDb == null)
                return NotFound();

            Mapper.Map(dogadjajDto, dogadjajInDb);
            //ovde stalno pravi problem
            _context.SaveChanges();
            dogadjajInDb.IdKaOrganizaciji = User.Identity.GetUserId();
            _context.SaveChanges();
            return Ok();

        }
        //DELETE /api/dogadjaji/1
        [HttpDelete]
        public IHttpActionResult ObrisiDogadjaj(int id)
        {
            var dogadjajInDb = _context.Dogadjaji.SingleOrDefault(d => d.Id == id);

            //sada nalazimo potrebnu robu za taj dogadajaj


            if (dogadjajInDb == null)
                return NotFound();

            var listaPotrebneRobe = _context.Roba.Where(r => r.DogadjajId == dogadjajInDb.Id).ToList();

            foreach (var roba in listaPotrebneRobe)
            {
                var prikupljena = _context.PrikupljenaRoba.Where(p => p.IdPotrebneRobe == roba.Id).ToList();
                foreach (var pr in prikupljena)
                    _context.PrikupljenaRoba.Remove(pr);
                _context.Roba.Remove(roba);
                //_context.SaveChanges();
            }
            
            _context.Dogadjaji.Remove(dogadjajInDb);
            _context.SaveChanges();
            return Ok();

        }
    }
}
