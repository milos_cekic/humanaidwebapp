﻿using AutoMapper;
using HumanAID.Dtos;
using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HumanAID.Controllers.Api
{
    public class PotrebanNovacController : ApiController
    {
        private ApplicationDbContext _context;
        public PotrebanNovacController()
        {
            _context = new ApplicationDbContext();
        }

        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}
        //GET /api/potrebannovac
        [HttpGet]
        public IEnumerable<PotrebanNovacDto> VratiNovce()
        {
            return _context.PotrebanNovac.ToList().Select(Mapper.Map<PotrebanNovac, PotrebanNovacDto>);
        }
        //GET /api/potrebannovac/1
        [HttpGet]
        public IHttpActionResult VratiPotrebanNovac(int id)
        {
            var novac = _context.PotrebanNovac.SingleOrDefault(r => r.Id == id);

            if (novac == null)
                return NotFound();

            return Ok(Mapper.Map<PotrebanNovac, PotrebanNovacDto>(novac));
        }

        //post metod ne treba iz razloga sto se potreban novac uvek kreira kada
        //se kreira i dogadjaja makar novac bio null

        ////PUT /api/robe/1
        //[HttpPut]
        //public IHttpActionResult AzurirajProizvod(int id, RobaDto robaDto)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest();

        //    var robaInDb = _context.Roba.SingleOrDefault(r => r.Id == id);

        //    if (robaInDb == null)
        //        return NotFound();

        //    Mapper.Map(robaDto, robaInDb);

        //    _context.SaveChanges();
        //    return Ok();

        //}
        //za put cemo realno da vidimo da li nam je potreban
        //ali po mojim trenutnim shvatanjima uvek ce put da se radi u drugim controllerima
        //kada upisujemo/brisemo robu i kada imamo donacije oba tipa

        //delete isto nam ne treba i necemo ga pozivati jer ce se on pozivati
        //kada se brise dogadjaj zapravo jer to ide kaskadno

        //DELETE /api/potrebannovac/1
        [HttpDelete]
        public IHttpActionResult ObrisiPotrebanNovac(int id)
        {
            var novacInDb = _context.PotrebanNovac.SingleOrDefault(r => r.Id == id);

            if (novacInDb == null)
                return NotFound();

            _context.PotrebanNovac.Remove(novacInDb);
            _context.SaveChanges();
            return Ok();

        }

    }
}
