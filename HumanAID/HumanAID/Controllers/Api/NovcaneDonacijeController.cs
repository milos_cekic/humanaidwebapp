﻿using AutoMapper;
using HumanAID.Dtos;
using HumanAID.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HumanAID.Controllers.Api
{
    public class NovcaneDonacijeController : ApiController
    {
        private ApplicationDbContext _context;

        public NovcaneDonacijeController()
        {
            _context = new ApplicationDbContext();
        }
        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}

        //GET /api/novcanedonacije
        [HttpGet]
        public IEnumerable<NovcaneDonacijeDto> VratiDonacije()
        {
            return _context.NovcaneDonacije.ToList().Select(Mapper.Map<NovcaneDonacije, NovcaneDonacijeDto>);
        }
        //GET /api/novcanedonacije/1
        [HttpGet]
        public IHttpActionResult VratiDonaciju(int id)
        {
            var donacija = _context.NovcaneDonacije.SingleOrDefault(r => r.Id == id);

            if (donacija == null)
                return NotFound();

            return Ok(Mapper.Map<NovcaneDonacije, NovcaneDonacijeDto>(donacija));
        }

        // POST /api/novcanedonacije
        [HttpPost]
        public IHttpActionResult NovaDonacija(NovcaneDonacijeDto donacijaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            
            //prvo proveravamo da li postoji zapis o sponzoru i dogadjaju u Donira
            var donira = _context.Donira.SingleOrDefault(d => d.IdDogadjaja == donacijaDto.IdDogadjaja && d.IdSponzora == donacijaDto.IdSponzora);
            if (donira == null)
            {
                Donira doniraAdd = new Donira
                {
                    IdDogadjaja = donacijaDto.IdDogadjaja,
                    IdSponzora = User.Identity.GetUserId()
                };
                _context.Donira.Add(doniraAdd);
                _context.SaveChanges();
            }
            
            //sada treba iz potrebnog novca da povecamo potreban novac za onoliko koliko je donirano
            //takodje treba i da povecamo trenutnoStanjeNovca koje organizacija poseduje
            //a nalazi se kod potrebnog novca

            var novac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == donacijaDto.IdPotrebnogNovca);
            if (novac != null)
            {
                novac.TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje += donacijaDto.KolicinaNovca;
                novac.UkupnoPrikupljenaKolicinaNovca += donacijaDto.KolicinaNovca;
            }
            //svakako se pamte donacije nezavisno od potrebnog novca, ali ukoliko  ima
            //potrebnog novca uticu na isti

            var donacija = Mapper.Map<NovcaneDonacijeDto, NovcaneDonacije>(donacijaDto);
            _context.NovcaneDonacije.Add(donacija);
            _context.SaveChanges();

            donacijaDto.Id = donacija.Id;
            return Created(new Uri(Request.RequestUri + "/" + donacija.Id), donacijaDto);

        }

        //nema poetene da postoji PUT


        //DELETE /api/prikupljenerobe/1
        [HttpDelete]
        public IHttpActionResult ObrisiDonaciju(int id)
        {
            var donacijaInDb = _context.NovcaneDonacije.SingleOrDefault(r => r.Id == id);

            if (donacijaInDb == null)
                return NotFound();

            var novac = _context.PotrebanNovac.SingleOrDefault(n => n.Id == donacijaInDb.IdPotrebnogNovca);
            if (novac != null)
            {
                novac.TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje -= donacijaInDb.KolicinaNovca;
                novac.UkupnoPrikupljenaKolicinaNovca -= donacijaInDb.KolicinaNovca;
            }


            _context.NovcaneDonacije.Remove(donacijaInDb);
            _context.SaveChanges();

            
            //zatim logika da li da se obrise i iz Donira
            var listaPrikupljenihRobaSponzora = _context.PrikupljenaRoba.Where(r => r.IdMogucegSponzora == donacijaInDb.IdSponzora).ToList();
            var listaDonacijaZaSponzora = _context.NovcaneDonacije.Where(d => d.IdSponzora == donacijaInDb.IdSponzora).ToList();
            if (listaPrikupljenihRobaSponzora == null && listaDonacijaZaSponzora==null)
            {
                var donira = _context.Donira.SingleOrDefault(d => d.IdDogadjaja == donacijaInDb.IdDogadjaja && d.IdSponzora == donacijaInDb.IdSponzora);
                if(donira!=null)
                {
                    _context.Donira.Remove(donira);
                    _context.SaveChanges();
                }
            }

            return Ok();
        }


    }
}
