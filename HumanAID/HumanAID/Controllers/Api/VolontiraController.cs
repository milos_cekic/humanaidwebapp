﻿using AutoMapper;
using HumanAID.Dtos;
using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HumanAID.Controllers.Api
{
    public class VolontiraController : ApiController
    {
        private ApplicationDbContext _context;
        public VolontiraController()
        {
            _context = new ApplicationDbContext();
        }
        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}
        //GET /api/volontira
        [HttpGet]
        public IEnumerable<VolontiraDto> VratiVolontira()
        {
            return _context.Volontira.ToList().Select(Mapper.Map<Volontira, VolontiraDto>);
        }

        //id je id dogadjaja, vracaju se volonteri na dogadjaju preko id-a
        //GET /api/volontira/1
        [HttpGet]
        public IHttpActionResult VratiVolontira(int id)
        {
            var volontira = _context.Volontira.SingleOrDefault(v => v.Id == id);

            if (volontira == null)
                return NotFound();

            return Ok(Mapper.Map<Volontira, VolontiraDto>(volontira));
        }

        // POST /api/volontira
        [HttpPost]
        public IHttpActionResult NoviVolontira(VolontiraDto volontiraDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            int dogadjajId = volontiraDto.IdDogadjaja;

            var dogadjaj = _context.Dogadjaji.SingleOrDefault(d => d.Id == dogadjajId);
            if (dogadjaj.MozeDaSePovecaBrTrenutnihVolontera())
            {
                dogadjaj.BrojTrenutnihVolontera++;
                var volontira = Mapper.Map<VolontiraDto, Volontira>(volontiraDto);
                _context.Volontira.Add(volontira);
                _context.SaveChanges();

                //ovde cemo da ubacimo logiku ako saznamo kako se salje generisan mejl npr. organiaciji
                //da se novi volonter prijavio za volontiranje
                return Created(new Uri(Request.RequestUri + "/" + volontira.Id), volontiraDto);
            }
            else
                return BadRequest();
        }

        //DELETE /api/volontira/1
        [HttpDelete]
        public IHttpActionResult ObrisiVolontira(int id)
        {
            var volontiraInDb = _context.Volontira.SingleOrDefault(v => v.Id == id);

            if (volontiraInDb == null)
                return NotFound();

            var dogadjaj = _context.Dogadjaji.SingleOrDefault(d => d.Id == volontiraInDb.IdDogadjaja);

            //tehnicki nikad ne bi moglo da se upise za volontiranje ako ne moze da se poveca broj trentnih
            //ali cisto da imamo proveru na obe strane
            if (dogadjaj.MozeDaSeSmanjiBrTrenutnihVolontera())
            {
                dogadjaj.BrojTrenutnihVolontera--;
                _context.Volontira.Remove(volontiraInDb);

                _context.SaveChanges();
                return Ok();
            }
            else
                return BadRequest("Ne moze da se izbrise volontiranje");
        }

    }
}
