﻿using AutoMapper;
using HumanAID.Dtos;
using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HumanAID.Controllers.Api
{
    public class DoniraController : ApiController
    {
        private ApplicationDbContext _context;
        public DoniraController()
        {
            _context = new ApplicationDbContext();
        }
        ////mora i dispose
        //protected override void Dispose(bool disposing)
        //{
        //    _context.Dispose();
        //}
        //GET /api/DONIRA
        [HttpGet]
        public IEnumerable<DoniraDto> VratiDonira()
        {
            return _context.Donira.ToList().Select(Mapper.Map<Donira, DoniraDto>);
        }

        //id je id dogadjaja, vracaju se sponzori na dogadjaju preko id-a
        //GET /api/donira/1
        [HttpGet]
        public IHttpActionResult VratiDonira(int id)
        {
            var donira = _context.Donira.SingleOrDefault(d => d.Id == id);

            //morali smo da nadjemo dogadjaj zbog provere
            if (donira == null)
                return NotFound();

            return Ok(Mapper.Map<Donira, DoniraDto>(donira));
        }

        // POST /api/donira
        [HttpPost]
        public IHttpActionResult NoviDonira(DoniraDto doniraDto)
        {
            //doniraDto ce u sebi da ima samo Id-jeve stranih kljuceva
            if (!ModelState.IsValid)
                return BadRequest();


            var donira = Mapper.Map<DoniraDto, Donira>(doniraDto);
            _context.Donira.Add(donira);
            _context.SaveChanges();

            //obrati paznju na ovo
            return Created(new Uri(Request.RequestUri + "/" + donira.Id), doniraDto);

        }

        //DELETE /api/donira/1
        [HttpDelete]
        public IHttpActionResult ObrisiDonira(int id)
        {
            var doniraInDb = _context.Donira.SingleOrDefault(d => d.Id == id);

            if (doniraInDb == null)
                return NotFound();


            //poenta je da kad se obrise torka iz donira
            //treba i da se obrise sta je sve taj sponzor donirao
            //to se radi kod prekida sponzorstva, on se brise odavde
            //i treba da se brise sve sto je donirao za taj dogadjaj
            //dogadjajID cemo odavde da izvucemo i ovde(verovatno) da razradimo logiku
            //ostalog brisanja

            //ne treba singleOrDefault jer se brise kaskadno
            var dogadjaj = _context.Dogadjaji.Single(d => d.Id == doniraInDb.IdDogadjaja);
            //posto novcana sponzorstva jos nemamo uvedeno brisemo svu doniranu robu za tog sponzora

            var PrikupljenaRobaSponzora = _context.PrikupljenaRoba.Where(r => r.IdDogadjaja == dogadjaj.Id && r.IdMogucegSponzora == doniraInDb.IdSponzora).ToList();
            //sad za svaku donaciju robnu brisemo je iz baze, ako postoji naravno
            if (PrikupljenaRobaSponzora != null)
            {
                foreach (var pribavljena in PrikupljenaRobaSponzora)
                {
                    var potrebna = _context.Roba.SingleOrDefault(p => p.Id == pribavljena.IdPotrebneRobe);
                    if(potrebna!=null)
                    {
                        potrebna.PrikupljenaRoba -= pribavljena.Kolicina;
                        _context.SaveChanges();
                    }
                    _context.PrikupljenaRoba.Remove(pribavljena);
                    _context.SaveChanges();
                }
            }

            //zatim brisemo novcana sponzorstva
            var PrikupljenNovacSponzora = _context.NovcaneDonacije.Where(r => r.IdDogadjaja == dogadjaj.Id && r.IdSponzora == doniraInDb.IdSponzora).ToList();
            //sad za svaku donaciju robnu brisemo je iz baze, ako postoji naravno
            if (PrikupljenNovacSponzora != null)
            {
                foreach (var donacija in PrikupljenNovacSponzora)
                    _context.NovcaneDonacije.Remove(donacija);
                _context.SaveChanges();
            }
            
            //i na kraju kada pobrisemo sve te stvari izbrisemo i sam zapis iz Donira
            //generalno nama Donira sluzi da se vidi ko su sponzori na dogadjaju, nebitno kada je u pripremi ko je koliko donirao
            //kasnije kada je aktuelan ili prosli mozemo da prikazemo listu prikupljene ili da sracunamo iz donira ko je sta koliko donirao
            _context.Donira.Remove(doniraInDb);
            _context.SaveChanges();
            //proveriti da li je dovoljan jedan context, mada logicno je da jeste
            return Ok();

        }


    }
}
