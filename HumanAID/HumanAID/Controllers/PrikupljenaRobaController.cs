﻿using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HumanAID.Controllers
{
    public class PrikupljenaRobaController : Controller
    {
        private ApplicationDbContext _context;
        public PrikupljenaRobaController()
        {
            _context = new ApplicationDbContext();
        }
        //mora i dispose
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: PrikupljenaRoba
        public ActionResult Index()
        {
            return View();
        }
        [Route("prikupljenaroba/novaprikupljena/{idPotrebneRobe}/{idDogadjaja}/{naziv}")]
        public ActionResult NovaPrikupljena(int idPotrebneRobe,int idDogadjaja, string naziv)
        {
            ViewBag.idPotrebneRobe = idPotrebneRobe;
            ViewBag.idDogadjaja = idDogadjaja;
            ViewBag.naziv = naziv;
            return View(ViewBag);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("prikupljenaroba/sacuvajprikupljenurobu")]
        public ActionResult SacuvajPrikupljenuRobu(PrikupljenaRoba roba)
        {
            if (!ModelState.IsValid)
            {
                return View("NovaPrikupljena", roba);
            }

            if (roba.Id == 0)
            {
                //ovde negde cemo da imamo pitanje ko je prikupio tj. po tome kako je donirana roba
                //i uzmemo id trenutno ulogovanog sponzora

                //i onda treba i da pitamo da li je ovaj sponzor prvi put donirao robu na ovom dogadjaju ako jeste
                //onda ga dodajemo u tabeli Donira

                var potrebnaRoba = _context.Roba.Single(r => r.Id == roba.IdPotrebneRobe);
                potrebnaRoba.PrikupljenaRoba += roba.Kolicina;
                _context.Roba.Add(potrebnaRoba);

                //i konacno dodajemo u pribavljenoj robi
                _context.PrikupljenaRoba.Add(roba);
            }
            //else
            //{
            //    var robaInDb = _context.Roba.Single(r => r.Id == roba.Id);

            //    robaInDb.Proizvod = roba.Proizvod;
            //    robaInDb.PotrebnaKolicina = roba.PotrebnaKolicina;
            //    //misli se na trenutnu kolicinu
            //    var listaPrikupljeneRobeZaPotrebnu = _context.PrikupljenaRoba.Where(p => p.IdPotrebneRobe == robaInDb.Id);
            //    int trenutna = 0;
            //    foreach (PrikupljenaRoba prikupljena in listaPrikupljeneRobeZaPotrebnu)
            //        trenutna += prikupljena.Kolicina;

            //    robaInDb.PrikupljenaRoba = trenutna;
            //    robaInDb.CenaProizvodaPoKomadu = roba.CenaProizvodaPoKomadu;
            //    robaInDb.DogadjajId = roba.DogadjajId;

            //}
            //trenutno nam ne treba jer ne znamo za sada da li ce biti menjanja

            _context.SaveChanges();
            //            return RedirectToAction("PrikazDogadjaja", "Dogadjaj", roba.IdDogadjaja);
            return RedirectToAction("Index", "PrikupljenaRoba");
        }
    }
}