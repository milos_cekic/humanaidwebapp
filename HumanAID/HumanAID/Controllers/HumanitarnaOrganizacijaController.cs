﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HumanAID.Models;
using HumanAID.ViewModels;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.IO;

namespace HumanAID.Controllers
{
    public class HumanitarnaOrganizacijaController : Controller
    {
        private ApplicationDbContext _context;
        public HumanitarnaOrganizacijaController()
        {
            _context = new ApplicationDbContext();
        }
        //mora i dispose
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: HumanitarnaOrganizacija
        [Route("organizacija/index")]
        public ActionResult Index()
        {

            var viewModel = new SveOrganizacijeViewModel
            {
                ListaOrganizacija = _context.Users.Where(u => u.KorisnikTip == TipKorisnika.Organizacija).ToList()
            };


            string userID = User.Identity.GetUserId();
            if (userID != null)
            {
                //korisnik je ulogovan i proveravamo da li je admin
                var user = _context.Users.Single(u => u.Id == userID);
                if (user.Administrator)

                    return View("SveOrganizacijeAdmin",viewModel);
            }
            //ako nije admin vratice se svakako klasican view
            return View(viewModel);
        }

        //[Route("organizacija/novaorganizacija")]
        //public ActionResult NovaOrganizacija()
        //{
        //    return View("OrganizacijaForm");
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Route("organizacija/sacuvajorganizaciju")]
        //public ActionResult SacuvajOrganizaciju(HumanitarnaOrganizacija organizacija)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View("OrganizacijaForm", organizacija);
        //    }


        //    if (organizacija.Id == 0)
        //    {
        //        //to je nova organizacija
        //        //ovo ovde trenutno mora ovako dok ne sredimo konacnu formu za registraciju
        //        organizacija.DatumRegistracijeKorisnika = DateTime.Now;
        //        _context.HumanitarneOrganizacije.Add(organizacija);
        //    }
        //    else
        //    {
        //        var organizacijaInDb = _context.HumanitarneOrganizacije.Single(o => o.Id == organizacija.Id);
        //        //kasnije cemo da napravimo mozda UpdateOrganizacijuDTO zbog sigurnosnih propusta mappera
        //        //Mapper.Map da probamo kasnije
        //        organizacijaInDb.Email = organizacija.Email;
        //        organizacijaInDb.Username = organizacija.Username;
        //        organizacijaInDb.NazivOrganizacije = organizacija.NazivOrganizacije;
        //        organizacijaInDb.DatumOsnivanjaOrganizacije = organizacija.DatumOsnivanjaOrganizacije;
        //        organizacijaInDb.GlavniRacun = organizacija.GlavniRacun;
        //        organizacijaInDb.Kontakt = organizacija.Kontakt;
        //        organizacijaInDb.Adresa = organizacija.Adresa;
        //        organizacijaInDb.Grad = organizacija.Grad;
        //        organizacijaInDb.Opis = organizacija.Opis;
        //        organizacijaInDb.LinkKaSajtu = organizacija.LinkKaSajtu;
        //        organizacijaInDb.LogoPutanja = organizacija.LogoPutanja;

        //    }
        //    _context.SaveChanges();
        //    //sada je problem sa index stranom, tj. ne redirectuje kako treba
        //    //ali upisuje u bazu kako treba
        //    return RedirectToAction("Index", "HumanitarnaOrganizacija");

        //}

        [Route("organizacija/profil/{id}/{vidljivost}")]
        public ActionResult PrikaziProfilOrganizacije(string id, bool vidljivost)
        //id humanitarne organizacije
        {
            var organizacija = _context.Users.SingleOrDefault(c => c.Id == id);

            if (organizacija == null)
                return HttpNotFound();
            if(vidljivost==true)
            {
                var organizovaniDogadjaji = _context.Dogadjaji.Where(c => c.IdKaOrganizaciji == id).ToList();
                List<bool> mozeDaKupi = new List<bool>(organizovaniDogadjaji.Count());
                bool provera = false;
                foreach (var dogadjaj in organizovaniDogadjaji)
                {
                    var novac = _context.PotrebanNovac.SingleOrDefault(n => n.IdDogadjaja == dogadjaj.Id);
                    if (novac != null)
                        provera = novac.DaLIMozeDaSeKupiSvaRoba();

                    if (dogadjaj.Faza == FazaDogadjaja.UPripremi && provera == true)
                        mozeDaKupi.Add(true);
                    else
                        mozeDaKupi.Add(false);
                }


                var viewModel = new ProfilOrganizacijeViewModel
                {
                    Organizacija = organizacija,
                    ListaOrganizovanihDogadjaja = organizovaniDogadjaji,
                    MozeDaSeKupiOstatak = mozeDaKupi
                };

                return View("ProfilOrganizacijeSvoj",viewModel);
            }
            else
            {
                    var organizovaniDogadjaji = _context.Dogadjaji.Where(c => c.IdKaOrganizaciji == id).ToList();
                    //List<bool> mozeDaKupi = new List<bool>(organizovaniDogadjaji.Count());
                    //foreach (var dogadjaj in organizovaniDogadjaji)
                    //{
                    //    var novac = _context.PotrebanNovac.SingleOrDefault(n => n.IdDogadjaja == dogadjaj.Id);
                    //    if (novac != null)
                    //        provera = novac.DaLIMozeDaSeKupiSvaRoba();

                    //    if (dogadjaj.Faza == FazaDogadjaja.UPripremi && provera == true)
                    //        mozeDaKupi.Add(true);
                    //    else
                    //        mozeDaKupi.Add(false);
                    //}


                    var viewModel = new ProfilOrganizacijeViewModel
                    {
                        Organizacija = organizacija,
                        ListaOrganizovanihDogadjaja = organizovaniDogadjaji,
                        MozeDaSeKupiOstatak = null
                    };

                    return View("ProfilOrganizacijeOstali", viewModel);
            }

        }

        [Route("organizacija/izmeni/{id}")]
        public ActionResult Izmeni(string id)
        {
            var organizacija = _context.Users.SingleOrDefault(o => o.Id == id);
            if (organizacija == null)
                return HttpNotFound();

            return View("OrganizacijaForm", organizacija);
        }





        [HttpPost]
        [Route("organizacija/novaslika")]
        public ActionResult SlikaNova(HttpPostedFileBase file, string id)
        {


            string putanja = "";
            string idOrganizacije = id;
            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/images/"), fileName);
                file.SaveAs(path);
                // sada kada ubacimo sliku treba da sacuvamo tu sliku
                putanja = fileName;
            }
            //ar userID = User.Identity.GetUserId();
            var organizacija = _context.Users.SingleOrDefault(d => d.Id == idOrganizacije);
            if (organizacija != null)
            {
                organizacija.LogoPutanja = putanja;
                _context.SaveChanges();
                
            }
            if (organizacija != null)
            {
                return RedirectToAction("PrikaziProfilOrganizacije", "HumanitarnaOrganizacija", new { id = organizacija.Id, vidljivost = true });
            }

            return RedirectToAction("PrikaziPoFazi", "Dogadjaj", new { faza = FazaDogadjaja.Aktuelni });
        }



    }
}