﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HumanAID.Dtos
{
    public class HumanitarnaOrganizacijaDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše korisničko ime.")]
        [StringLength(35)]
        public String Username { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vašu email adresu.")]
        [StringLength(100)]
        public String Email { get; set; }


        [Required(ErrorMessage = "Molimo Vas unesite Vaš broj telefona.")]
        [StringLength(255)]
        public String Kontakt { get; set; }

        public String Adresa { get; set; }

        public String Grad { get; set; }

        public String Opis { get; set; }

        public String LinkKaSajtu { get; set; }

        //cuva se putanja do te slike
        public String LogoPutanja { get; set; }

        public DateTime DatumRegistracijeKorisnika { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite naziv humanitarne organizacije.")]
        [StringLength(255)]
        public String NazivOrganizacije { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite datum osnivanja humanitarne organizacije.")]
        public DateTime DatumOsnivanjaOrganizacije { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite glavni račun humanitarne organizacije.")]
        [StringLength(255)]
        public String GlavniRacun { get; set; }
    }
}