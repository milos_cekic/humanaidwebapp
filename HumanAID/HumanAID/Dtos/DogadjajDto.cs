﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using HumanAID.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace HumanAID.Dtos
{
    public class DogadjajDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite naziv humanitarnog događaja.")]
        [Display(Name = "Naziv humanitarnog događaja")]
        [StringLength(255)]
        public String NazivDogadjaja { get; set; }

        [Display(Name = "Mesto održavanja")]
        public String MestoOdrzavanja { get; set; }

        [Display(Name = "Datum početka događaja")]
        [Required(ErrorMessage = "Molimo Vas unesite datum početka događaja.")]
        public DateTime DatumPocetkaDogadjaja { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite datum završetka događaja.")]
        [Display(Name = "Datum završetka događaja")]
        public DateTime DatumKrajaDogadjaja { get; set; }


        public virtual IEnumerable<Roba> PotrebnaRoba { get; set; }

        public int? BrojPotrebnihVolontera { get; set; }
        public int? BrojTrenutnihVolontera { get; set; }

        [Display(Name = "Žiro račun za realizaciju događaja")]
        //ovaj ziro racun je striktno vezan za dogadjaj, za organizaciju dogadjaja ili neka akcija
        public String ZiroRacun { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite opis humanitarnog događaja.")]
        [Display(Name = "Opis događaja")]
        public String Opis { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite fazu u kojoj se nalazi humanitarni događaj.")]
        [Display(Name = "Faza u kojoj se nalazi događaj")]
        public FazaDogadjaja Faza { get; set; }

        public String IdKaOrganizaciji { get; set; }


        [ForeignKey("IdKaOrganizaciji")]
        public virtual ApplicationUser Organizacija { get; set; }

        public int? IdPotrebnogNovca { get; set; }

        //treba da sredimo u dogadjaj controlleru za postavljanje,update i brisanje dogadjaja
        public DateTime? DatumPostavljanja { get; set; }

        public string UrlSlike { get; set; }

        [NotMapped]
        public HttpPostedFileBase UploadImage { get; set; }
    }
}