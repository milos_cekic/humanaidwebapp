﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace HumanAID.Dtos
{
    public class VolontiraDto
    {
        public int Id { get; set; }

        public String IdVolontera { get; set; }
        
        public int IdDogadjaja { get; set; }


        public virtual UserDto Volonter { get; set; }
        public virtual DogadjajDto Dogadjaj { get; set; }

    }
}