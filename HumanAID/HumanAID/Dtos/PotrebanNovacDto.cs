﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HumanAID.Dtos
{
    public class PotrebanNovacDto
    {
        public int Id { get; set; }

        public int IdDogadjaja { get; set; }

        [ForeignKey("IdDogadjaja")]
        public virtual DogadjajDto Dogadjaj { get; set; }

        public decimal? PotrebnaKolicinaNovca { get; set; }

        //ovo je sa strane novcanih donacija sponzora
        //kada se dodaje menja se ovo i kada se brise menja se ovo
        public decimal? UkupnoPrikupljenaKolicinaNovca { get; set; }

        //ovo je sa strane kako org trosi taj novac, u pocetku pre nego sto org pocne da ga trosi
        //bice jednak sa ukupnoPirkupljenim i zapravo kao sto  sa strane novcanih donacija uticu na ukupnoprikupljene
        //takodje uticu i na ovaj parametar, razlika je samo sto na ovaj parametar utice i org
        public decimal? TrenutnaKolicinaNovcaKojuOrganizacijaPoseduje { get; set; }

    }
}