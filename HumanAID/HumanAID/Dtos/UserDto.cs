﻿using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HumanAID.Dtos
{

    public class UserDto
    {

        public String Id { get; set; }
        [Required(ErrorMessage ="Molimo Vas unesite email")]
        public String Email { get; set; }
        [Required(ErrorMessage = "Molimo Vas unesite broj telefona")]
        public String PhoneNumber { get; set; }
        [Required(ErrorMessage = "Molimo Vas unesite username")]
        public String UserName { get; set; }

        [Required(ErrorMessage = "Molimo Vas odaberite tip korisnika.")]
        public TipKorisnika KorisnikTip { get; set; }

        //administrator ima iste propertije kao i volonter: ime, prezime, datumRodjenja i pol
        //postojace samo jedan administrator i ovo je flag za njega
        public bool Administrator { get; set; }
        //on ce uvek da bude false samo prvi put kada budemo ubacivali adminisratora bice true

        //pocinje abstract Korisnik
        public String Adresa { get; set; }

        public String Grad { get; set; }

        public String Opis { get; set; }

        [Display(Name = "Link ka sajtu")]
        public String LinkKaSajtu { get; set; }

        //cuva se putanja do te slike, ovo cemo da vidimo kako cemo da resimo sliku
        //to cemo naknadno da resimo
        [Display(Name = "Putanja do Vaše slike")]
        public String LogoPutanja { get; set; }

        public DateTime DatumRegistracijeKorisnika { get; set; }
        //do ovde je bila abstract Korisnik klasa, svi imaju ove zajednicke propertije

        //odavde ide za humanitarnu organizaciju

        [StringLength(255)]
        [Display(Name = "Naziv humanitarne organizacije")]
        public String NazivOrganizacije { get; set; }

        [Display(Name = "Datum osnivanja humanitarne organizacije")]
        public DateTime? DatumOsnivanjaOrganizacije { get; set; }

        [StringLength(255)]
        [Display(Name = "Glavni račun humanitarne organizacije")]
        public String GlavniRacun { get; set; }
        //do ovde je bila humanitarna organizacija

        //odavde pocinje sponzor
        [StringLength(100)]
        [Display(Name = "Naziv kompanije")]
        public String Naziv { get; set; }

        [Display(Name = "Datum osnivanja kompanije")]
        public DateTime? DatumOsnivanja { get; set; }

        [StringLength(255)]
        [Display(Name = "Delatnost kojom se bavite")]
        public String Delatnost { get; set; }
        //do odve je bio sponzor
        //pocinje volonter

        [StringLength(25)]
        public String Ime { get; set; }

        [StringLength(35)]
        public String Prezime { get; set; }

        [Display(Name = "Datum rodjenja")]
        public DateTime? DatumRodjenja { get; set; }

        [Display(Name = "Izaberite pol")]
        public String Pol { get; set; }
    }
}