﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HumanAID.Dtos
{
    public class RobaDto
    {
        public int Id { get; set; }

        //strani kljuc ka dogadjaju
        public int DogadjajId { get; set; }

        public virtual DogadjajDto Dogadjaj { get; set; }

        [Required]
        public String Proizvod { get; set; }

        public int? PrikupljenaRoba { get; set; }

        [Required]
        public int PotrebnaKolicina { get; set; }

        [Required]
        public decimal CenaProizvodaPoKomadu { get; set; }

        public int? IdPotrebnogNovca { get; set; }

        [ForeignKey("IdPotrebnogNovca")]
        public virtual PotrebanNovacDto PotrebanNovac { get; set; }

    }
}