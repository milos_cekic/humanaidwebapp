﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HumanAID.Dtos
{
    public class NovcaneDonacijeDto
    {
        public int Id { get; set; }

        public string IdSponzora { get; set; }

        public int IdDogadjaja { get; set; }

        public int IdPotrebnogNovca { get; set; }

        //zato sto samo sponzor moze novcano da donira
        public string NazivSponzora { get; set; }

        public decimal KolicinaNovca { get; set; }

        [ForeignKey("IdSponzora")]
        public virtual UserDto Sponzor { get; set; }

        [ForeignKey("IdDogadjaja")]
        public virtual DogadjajDto Dogadjaj { get; set; }

        [ForeignKey("IdPotrebnogNovca")]
        public virtual PotrebanNovacDto PotrebanNovac { get; set; }

    }
}