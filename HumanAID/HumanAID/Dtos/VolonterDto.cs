﻿using HumanAID.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HumanAID.Dtos
{
    public class VolonterDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše korisničko ime.")]
        [StringLength(35)]
        public String Username { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vašu email adresu.")]
        [StringLength(100)]
        public String Email { get; set; }


        [Required(ErrorMessage = "Molimo Vas unesite Vaš broj telefona.")]
        [StringLength(255)]
        public String Kontakt { get; set; }

        public String Adresa { get; set; }

        public String Grad { get; set; }

        public String Opis { get; set; }

        public String LinkKaSajtu { get; set; }

        //cuva se putanja do te slike
        public String LogoPutanja { get; set; }

        public DateTime DatumRegistracijeKorisnika { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše ime.")]
        [StringLength(25)]
        public String Ime { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše prezime.")]
        [StringLength(35)]
        public String Prezime { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite datum rodjenja.")]
        [Min16GodinaVolonter]
        public DateTime DatumRodjenja { get; set; }

        [Display(Name = "Izaberite pol")]
        public String Pol { get; set; }

    }
}