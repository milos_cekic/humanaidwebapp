﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HumanAID.Models;
using System.ComponentModel.DataAnnotations;

namespace HumanAID.Dtos
{
    public class SponzorDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vaše korisničko ime.")]
        [StringLength(35)]
        public String Username { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite Vašu email adresu.")]
        [StringLength(100)]
        public String Email { get; set; }


        [Required(ErrorMessage = "Molimo Vas unesite Vaš broj telefona.")]
        [StringLength(255)]
        public String Kontakt { get; set; }

        public String Adresa { get; set; }

        public String Grad { get; set; }

        public String Opis { get; set; }

        public String LinkKaSajtu { get; set; }

        //cuva se putanja do te slike
        public String LogoPutanja { get; set; }

        public DateTime DatumRegistracijeKorisnika { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite naziv Vaše kompanije.")]
        [StringLength(100)]
        public String Naziv { get; set; }

        [Required(ErrorMessage = "Molimo Vas unesite datum osnivanja kompanije.")]
        public DateTime DatumOsnivanja { get; set; }


        [Required(ErrorMessage = "Molimo Vas unesite delatnost kojom se bavi Vaša kompanija.")]
        [StringLength(255)]
        public String Delatnost { get; set; }




    }
}