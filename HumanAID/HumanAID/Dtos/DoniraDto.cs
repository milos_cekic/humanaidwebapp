﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HumanAID.Dtos
{
    public class DoniraDto
    {
        public int Id { get; set; }

        public String IdSponzora { get; set; }

        public int IdDogadjaja { get; set; }


        public virtual UserDto Sponzor { get; set; }
        public virtual DogadjajDto Dogadjaj { get; set; }

    }
}