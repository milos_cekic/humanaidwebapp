﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HumanAID.Dtos
{
    public class PrikupljenaRobaDto
    {
        public int Id { get; set; }

        //naziv potrebne robe
        [Required]
        public String NazivPrikupljeneRobe { get; set; }

        [Required]
        public TipDoniranja DoniranaTip { get; set; }

        //ovde ide naziv organizcije(ako je kupljeno doniranim novcem)
        //ili naziv sponzora
        [Required]
        public String PrikupioRobu { get; set; }

        [Required]
        public int Kolicina { get; set; }

        public int IdPotrebneRobe { get; set; }

        public virtual RobaDto PotrebnaRoba { get; set; }

        public String IdMogucegSponzora { get; set; }

        [ForeignKey("IdMogucegSponzora")]
        public virtual UserDto Sponzor { get; set; }

        public int IdDogadjaja { get; set; }

        public virtual DogadjajDto Dogadjaj { get; set; }

    }
}