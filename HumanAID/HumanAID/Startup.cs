﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HumanAID.Startup))]
namespace HumanAID
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
